package com.daiyngroup.personal.qagazdaiyn;

/**
 * Created by adilkhan on 10/1/17.
 */

public class Config {
    public static final int EPAY_PAY_REQUEST = 9999;
    public static final int EPAY_PAY_SUCCESS = 1;
    public static final int EPAY_PAY_FAILURE = 0;

    /****               Debug log             ****/
    public static final String LOG_TAG = "EPAY_SDK";

    /****               Intent logs             ****/
    public static final String EMAIL_LOG = "EMAIL_LOG";

    /****               Signed order url             ****/
    public static final String SIGNED_ORDER_URL = "https://daiyn.com/mobile_order_epay.php";

    /****               Epay form loading url             ****/
    public static final String EPAY_POST_URL = "https://epay.kkb.kz/jsp/process/logon.jsp";

    /****               Epay result url             ****/
    public static final String EPAY_RESULT = "https://epay.kkb.kz/jsp/process/result.jsp";
    public static final String EPAY_RESULT_TEST = "https://testpay.kkb.kz/jsp/process/result.jsp";


    /****               Epay results link             ****/
    public static final String EPAY_BACK_URL = "https://daiyn.com/index.php";
    public static final String EPAY_FAILURE_BACK_URL = "https://daiyn.com/postlink.php";
}
