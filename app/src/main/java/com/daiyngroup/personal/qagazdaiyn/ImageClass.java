package com.daiyngroup.personal.qagazdaiyn;

class ImageClass {

    String imagePath;

    public ImageClass(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
