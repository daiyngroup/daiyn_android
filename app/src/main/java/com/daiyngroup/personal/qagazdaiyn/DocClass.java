package com.daiyngroup.personal.qagazdaiyn;

import java.io.File;

public class DocClass {

    private File file;
    private String fileType;

    public DocClass(File file, String fileType) {
        this.file = file;
        this.fileType = fileType;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
