package com.daiyngroup.personal.qagazdaiyn;

import java.io.File;

public class FileClass {


    private File file;
    private String fileType;
    private String name;
    private String[] attr;
    private String pageNum;

    public FileClass(File file, String fileType, String name, String[] attr, String pageNum) {
        this.file = file;
        this.fileType = fileType;
        this.name = name;
        this.attr = attr;
        this.pageNum = pageNum;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getAttr() {
        return attr;
    }

    public void setAttr(String[] attr) {
        this.attr = attr;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }
}
