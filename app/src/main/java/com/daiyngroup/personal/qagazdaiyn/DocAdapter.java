package com.daiyngroup.personal.qagazdaiyn;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class DocAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    private ArrayList<DocClass> list = new ArrayList<>();

    private Context context;

    private String[] nameString;

    public DocAdapter(Context context, ArrayList<DocClass> list) {
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.doc_item_list, null);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.imageView);
            viewHolder.imagePath = (TextView) convertView.findViewById(R.id.fileName);
            viewHolder.delete = (ImageView) convertView.findViewById(R.id.delete);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                notifyDataSetChanged();
            }
        });
        try {
                Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.doc);
                viewHolder.image.setImageBitmap(myBitmap);
                //nameString = list.get(position).getFilePath().split("/");
                viewHolder.imagePath.setText(nameString[nameString.length-1]);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView image;
        TextView imagePath;
        ImageView delete;
    }

}
