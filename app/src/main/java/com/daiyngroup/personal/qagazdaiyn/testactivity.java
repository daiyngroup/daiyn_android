package com.daiyngroup.personal.qagazdaiyn;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.iceteck.silicompressorr.SiliCompressor;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerConfigurationException;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;

public class testactivity extends AppCompatActivity implements View.OnClickListener{

    private RelativeLayout mainLayout;
    private FrameLayout frame_layout;

    private String imageName;

    private TextView fileName;
    private TextView copies_qty;
    private TextView img_number_qty;

    private SeekBar seekbar;

    private Bitmap myBitmap;

    private int finalWidth;
    private int finalHeight;

    private ImageButton copy;
    private ImageButton rotate;
    private ImageButton done;
    private ImageButton image_number;
    private ImageButton paddings;

    private boolean isRotated = true;
    private boolean isCopy = false;
    private boolean isMargin = true;

    private String[] nameString;

    GridView grid;
    List<testclass> list;
    private testadapter adapter;

    private int diffH;
    private int diffW;
    private int baseH;
    private int baseW;
    private int requestcode;
    private int gridWidth;
    private int copiesNumber;

    private double relw;
    private double relh;
    private double conrel;

    private String[] jsonData = new String[9];

    private String device_token;
    private String orientation;
    private String padding_state;

    private MaterialNumberPicker picker;
    private MaterialNumberPicker imgPicker;

    Context context;

    private int imgNumber;
    private int rotateState = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testactivity);

        fileName = (TextView) findViewById(R.id.fileName);
        copies_qty = (TextView) findViewById(R.id.copies_qty);
        img_number_qty = (TextView) findViewById(R.id.img_number_qty);

        seekbar = (SeekBar) findViewById(R.id.seekBar);

        copy = (ImageButton) findViewById(R.id.copy);
        rotate = (ImageButton) findViewById(R.id.orientation);
        done = (ImageButton) findViewById(R.id.done);
        image_number = (ImageButton) findViewById(R.id.img_number);
        paddings = (ImageButton) findViewById(R.id.paddings);

        grid = (GridView) findViewById(R.id.grid);

        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        frame_layout = (FrameLayout) findViewById(R.id.frame_layout);

        device_token = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        context = this;

        Intent intent = getIntent();
        copiesNumber = 1;
        imgNumber = 1;
        padding_state = "1";

        imageName = intent.getStringExtra("imageName");
        requestcode = intent.getIntExtra("requestcode", 0);

        nameString = imageName.split("/");

        fileName.setText(nameString[nameString.length-1]);

        File imgFile = new File(imageName);

        if (imgFile.exists()) {
            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }

        ViewTreeObserver vto = grid.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                grid.getViewTreeObserver().removeOnPreDrawListener(this);
                finalWidth = grid.getMeasuredWidth();
                setSeekBar();
                return true;
            }
        });

        list = new ArrayList<testclass>();

        /*if (myBitmap.getHeight() > myBitmap.getWidth()) {
            myBitmap = RotateBitmap(myBitmap, -90);
        }*/

        try {
            myBitmap = SiliCompressor.with(context).getCompressBitmap(imageName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < imgNumber; i++) {
            list.add(new testclass(imageName, myBitmap.getWidth(), myBitmap.getHeight(), myBitmap));
        }

        grid.setColumnWidth(myBitmap.getWidth());

        adapter = new testadapter(this, list);

        grid.setAdapter(adapter);

    }

    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void setSeekBar() {
        rotate.setOnClickListener(this);
        copy.setOnClickListener(this);
        done.setOnClickListener(this);
        image_number.setOnClickListener(this);
        paddings.setOnClickListener(this);

        orientation = "P";
        gridWidth = grid.getWidth();
        conrel = 1.0*myBitmap.getHeight()/myBitmap.getWidth();

//        grid.setLayoutParams(new FrameLayout.LayoutParams(grid.getWidth()-grid.getHorizontalSpacing(), ViewGroup.LayoutParams.MATCH_PARENT));

        Log.d("BitmapHeight", ""+myBitmap.getHeight());
        Log.d("BitmapWidth", ""+myBitmap.getWidth());

        int a;
        a = myBitmap.getWidth() - grid.getWidth();
        a = (int) (a * conrel);
        a = list.get(0).getDiffH() - a;

        int b;
        b = grid.getWidth() - myBitmap.getWidth();
        b = (int) (b*conrel);
        b = list.get(0).getDiffH() + b;

            if (myBitmap.getWidth() > grid.getWidth()) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setDiffW(grid.getWidth());
                    list.get(i).setDiffH(a);
                }
                adapter.notifyDataSetChanged();
            } else {
                if (myBitmap.getWidth() < grid.getWidth()) {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).setDiffW(grid.getWidth());
                        list.get(i).setDiffH(b);
                    }
                }
                adapter.notifyDataSetChanged();
            }

        seekbar.setMax(list.get(0).getDiffW());
        finalWidth = list.get(0).getDiffW();
        finalHeight = list.get(0).getDiffH();
        diffH = finalHeight;
        diffW = finalWidth;
        baseH = mainLayout.getHeight();
        baseW = mainLayout.getWidth();
        relw = 1.0*(diffW)/baseW;
        relh = 1.0*(diffH)/baseH;

        Log.d("diffW1", ""+diffW);
        Log.d("diffH1", ""+diffH);
        restoreSettings();
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d("conrel:", ""+conrel);
                diffW = (finalWidth-progress);
                diffH = (int) (finalHeight-progress*conrel);
               /* Log.d("diffW", ""+diffW);
                Log.d("diffH", ""+diffH);*/
                baseH = mainLayout.getHeight();
                baseW = mainLayout.getWidth();

                relw = 1.0*(diffW)/baseW;
                relh = 1.0*(diffH)/baseH;

                Log.d("relw", ""+relw);
                Log.d("relh", ""+relh);

                Log.d("finalheight", ""+finalHeight);
                Log.d("finalwidth", ""+finalWidth);

                Log.d("diffw", ""+diffW);
                Log.d("diffh", ""+diffH);

                Log.d("baseW", ""+baseW);
                Log.d("baseH", ""+baseH);
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setDiffH(diffH);
                    list.get(i).setDiffW(diffW);
                }
                adapter.notifyDataSetChanged();
                grid.setColumnWidth(finalWidth-progress);
                grid.requestLayout();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private void restoreSettings() {
        String[] settings = getIntent().getStringArrayExtra("saved_settings");
        if (settings[0].equals("1") && settings[1].equals("1") && settings[2].equals("1") && settings[3].equals("1") && settings[4].equals("1")) {
            Log.d("Initial settings", "First launch");
        } else {
            if (settings[0].equals("0")) {
                setPaddings();
            }
            imgNumber = Integer.parseInt(settings[1]);
            img_number_qty.setText(""+imgNumber);
            testclass tempClass;
            tempClass = list.get(0);
            list.clear();
            for (int i = 0; i < imgNumber; i++) {
                list.add(tempClass);
            }
            adapter.notifyDataSetChanged();

            if (settings[2].equals("0")) {
                changeOrientation();
            }

            copiesNumber = (Integer.parseInt(settings[3]));
            copies_qty.setText(copiesNumber+"");

            seekbar.setProgress(Integer.parseInt(settings[4]));
            diffW = (finalWidth-seekbar.getProgress());
            diffH = (int) (finalHeight-seekbar.getProgress()*conrel);

            baseH = mainLayout.getHeight();
            baseW = mainLayout.getWidth();

            relw = 1.0*(diffW)/baseW;
            relh = 1.0*(diffH)/baseH;

            for (int i = 0; i < list.size(); i++) {
                list.get(i).setDiffH(diffH);
                list.get(i).setDiffW(diffW);
            }
            adapter.notifyDataSetChanged();
            grid.setColumnWidth(finalWidth-seekbar.getProgress());
            grid.requestLayout();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.copy:
                copyButtonPressed();
                break;
            case R.id.orientation:
                changeOrientation();
                break;
            case R.id.done:
                //doneButtonPressed();
                doneClick();
                break;
            case R.id.img_number:
                chooseImageNumber();
                break;
            case R.id.paddings:
                setPaddings();
                break;
            default:
                break;
        }
    }

    private void setPaddings() {
        Log.d("mainlayout", ""+mainLayout.getWidth());
        seekbar.setProgress(0);
        if (isMargin) {
            seekbar.setMax(mainLayout.getWidth());
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) frame_layout.getLayoutParams();
            params.setMargins(0, 0, 0, 0);
            padding_state = "0";
            frame_layout.setLayoutParams(params);
            isMargin = false;
            paddings.setBackgroundResource(R.drawable.ic_with_padding);

            for (int i = 0; i < list.size(); i++) {
                list.get(i).setDiffH(myBitmap.getHeight());
                list.get(i).setDiffW(myBitmap.getWidth());
            }
            adapter.notifyDataSetChanged();

            int a;
            a = myBitmap.getWidth() - mainLayout.getWidth();
            a = (int) (a*conrel);
            a = list.get(0).getDiffH() - a;

            int b;
            b = mainLayout.getWidth() - myBitmap.getWidth();
            b = (int) (b*conrel);
            b = b + list.get(0).getDiffH();

            if (myBitmap.getWidth() > mainLayout.getWidth()) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setDiffW(mainLayout.getWidth());
                    list.get(i).setDiffH(a);
                }
                Log.d("list diffw", ""+list.get(0).getDiffW());
                Log.d("list diffh", ""+list.get(0).getDiffH());
                adapter.notifyDataSetChanged();
            } else {
                if (myBitmap.getWidth() <= mainLayout.getWidth()) {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).setDiffW(mainLayout.getWidth());
                        list.get(i).setDiffH(b);
                    }
                    Log.d("list diffw", ""+list.get(0).getDiffW());
                    Log.d("list diffh", ""+list.get(0).getDiffH());
                }
                adapter.notifyDataSetChanged();
            }

            finalWidth = list.get(0).getDiffW();
            finalHeight = list.get(0).getDiffH();
            diffH = finalHeight;
            diffW = finalWidth;
            relw = 1.0*(diffW)/baseW;
            relh = 1.0*(diffH)/baseH;
            grid.setColumnWidth(list.get(0).getDiffW());
            grid.requestLayout();
        } else {
            seekbar.setMax(gridWidth);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) frame_layout.getLayoutParams();
            int l, r, t, b;
            l = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 6, getResources().getDisplayMetrics());
            r = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 3, getResources().getDisplayMetrics());
            t = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 4, getResources().getDisplayMetrics());
            b = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 4, getResources().getDisplayMetrics());
            params.setMargins(l, t, r, b);
            padding_state = "1";
            frame_layout.setLayoutParams(params);
            isMargin = true;
            paddings.setBackgroundResource(R.drawable.ic_without_padding);
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setDiffH(myBitmap.getHeight());
                list.get(i).setDiffW(myBitmap.getWidth());
            }
            adapter.notifyDataSetChanged();

            int a;
            a = myBitmap.getWidth() - gridWidth;
            a = (int) (a*conrel);
            a = list.get(0).getDiffH() - a;

            int a1;
            a1 = gridWidth - myBitmap.getWidth();
            a1 = (int) (a1*conrel);
            a1 = a1 + list.get(0).getDiffH();

            if (myBitmap.getWidth() > gridWidth) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setDiffW(gridWidth);
                    list.get(i).setDiffH(a);
                }
                Log.d("list diffw", ""+list.get(0).getDiffW());
                Log.d("list diffh", ""+list.get(0).getDiffH());
                adapter.notifyDataSetChanged();
            } else {
                if (myBitmap.getWidth() <= gridWidth) {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).setDiffW(gridWidth);
                        list.get(i).setDiffH(a1);
                    }
                    Log.d("list diffw", ""+list.get(0).getDiffW());
                    Log.d("list diffh", ""+list.get(0).getDiffH());
                }
                adapter.notifyDataSetChanged();
            }

            finalWidth = list.get(0).getDiffW();
            finalHeight = list.get(0).getDiffH();
            diffH = finalHeight;
            diffW = finalWidth;
            relw = 1.0*(diffW)/baseW;
            relh = 1.0*(diffH)/baseH;
            Log.d("relwNew", ""+relw);
            Log.d("relhNew", ""+relh);
            grid.setColumnWidth(list.get(0).getDiffW());
            grid.requestLayout();
        }
    }

    private void chooseImageNumber() {
        imgPicker =  new MaterialNumberPicker.Builder(this)
                .minValue(1)
                .maxValue(99)
                .defaultValue(Integer.parseInt(img_number_qty.getText().toString()))
                .backgroundColor(Color.WHITE)
                .separatorColor(Color.TRANSPARENT)
                .textColor(Color.BLACK)
                .textSize(20)
                .enableFocusability(false)
                .wrapSelectorWheel(true)
                .build();

        new AlertDialog.Builder(this)
                .setTitle("Выберите количество картинок на листе")
                .setView(imgPicker)
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        testclass tempClass;
                        tempClass = list.get(0);
                        list.clear();
                        for (int i = 0; i < imgPicker.getValue(); i++) {
                            list.add(tempClass);
                        }
                        adapter.notifyDataSetChanged();
                        img_number_qty.setText(""+imgPicker.getValue());
                        imgNumber = imgPicker.getValue();
                    }
                }).create().show();
    }

    private void doneClick() {
        jsonData[0] = device_token;
        jsonData[1] = orientation;
        jsonData[2] = "5";
        jsonData[3] = "image";
        jsonData[4] = ""+copiesNumber;
        jsonData[5] = ""+relw;
        jsonData[6] = ""+relh;
        jsonData[7] = ""+imgNumber;
        jsonData[8] = ""+padding_state;
        Intent intent = new Intent();
        intent.putExtra("jsonvalues", jsonData);
        intent.putExtra("filetype", "image");

        String[] settings = new String[5];
        settings[0] = padding_state;
        settings[1] = ""+imgNumber;
        settings[2] = ""+rotateState;
        settings[3] = ""+copiesNumber;
        settings[4] = ""+seekbar.getProgress();

        intent.putExtra("imagesettings", settings);

        setResult(RESULT_OK, intent);
        finish();
    }

    private void changeOrientation() {
        if (rotateState == 1) {
            rotateState = 0;
        } else {
            rotateState = 1;
        }
        seekbar.setProgress(0);
        if (!isRotated) {
            myBitmap = RotateBitmap(myBitmap, -90);
            isRotated = true;
        } else {
            myBitmap = RotateBitmap(myBitmap, 90);
            isRotated = false;
        }
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setImage(myBitmap);
        }
        adapter.notifyDataSetChanged();

//        int temp = finalWidth;
//        finalWidth = finalHeight;
//        finalHeight = temp;
//
//        temp = diffW;
//        diffW = diffH;
//        diffH = temp;
//
//        conrel = 1.0*myBitmap.getHeight()/myBitmap.getWidth();
//
//        /*for (int i = 0; i < list.size(); i++) {
//            list.get(i).setDiffH(diffH);
//            list.get(i).setDiffW(diffW);
//        }*/
//
//        if (myBitmap.getWidth() > grid.getWidth()) {
//            for (int i = 0; i < list.size(); i++) {
//                list.get(i).setDiffW(grid.getWidth());
//                list.get(i).setDiffH((int) (list.get(i).getDiffH() - (myBitmap.getWidth() - grid.getWidth()) * conrel));
//            }
//            adapter.notifyDataSetChanged();
//        } else {
//            if (myBitmap.getWidth() < grid.getWidth()) {
//                for (int i = 0; i < list.size(); i++) {
//                    list.get(i).setDiffW(grid.getWidth());
//                    list.get(i).setDiffH((int) (list.get(i).getDiffH() + (grid.getWidth() - myBitmap.getWidth()
//                    ) * conrel));
//                }
//            }
//            adapter.notifyDataSetChanged();
//        }

       // image.setImageBitmap(myBitmap);

         for (int i = 0; i < list.size(); i++) {
            list.get(i).setDiffH(myBitmap.getHeight());
            list.get(i).setDiffW(myBitmap.getWidth());
        }
        adapter.notifyDataSetChanged();
        conrel = 1.0*myBitmap.getHeight()/myBitmap.getWidth();
        Log.d("list diffw1", ""+list.get(0).getDiffW());
        Log.d("list diffh1", ""+list.get(0).getDiffH());
        Log.d("conrel", ""+conrel);
        int a;
        a = myBitmap.getWidth() - gridWidth;
        a = (int) (a*conrel);
        a = list.get(0).getDiffH() - a;

        int b;
        b = gridWidth - myBitmap.getWidth();
        b = (int) (b*conrel);
        b = b + list.get(0).getDiffH();

        if (myBitmap.getWidth() > gridWidth) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setDiffW(gridWidth);
                list.get(i).setDiffH(a);
            }
            Log.d("list diffw", ""+list.get(0).getDiffW());
            Log.d("list diffh", ""+list.get(0).getDiffH());
            adapter.notifyDataSetChanged();
        } else {
            if (myBitmap.getWidth() <= gridWidth) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setDiffW(gridWidth);
                    list.get(i).setDiffH(b);
                }
                Log.d("list diffw", ""+list.get(0).getDiffW());
                Log.d("list diffh", ""+list.get(0).getDiffH());
            }
            adapter.notifyDataSetChanged();
        }

        finalWidth = list.get(0).getDiffW();
        finalHeight = list.get(0).getDiffH();
        diffH = finalHeight;
        diffW = finalWidth;

        Log.d("BitmapHeight", ""+myBitmap.getHeight());
        Log.d("BitmapWidth", ""+myBitmap.getWidth());
//        Log.d("list diffw", ""+list.get(0).getDiffW());
//        Log.d("list diffh", ""+list.get(0).getDiffH());
        Log.d("grid width", ""+grid.getWidth());

        adapter.notifyDataSetChanged();
        grid.setColumnWidth(list.get(0).getDiffW());
        grid.requestLayout();
        if (orientation.equals("P")) {
            orientation = "L";
        } else {
            orientation = "P";
        }
        isMargin = !isMargin;
        setPaddings();
    }

    private void copyButtonPressed() {
        picker =  new MaterialNumberPicker.Builder(this)
                .minValue(1)
                .maxValue(99)
                .defaultValue(Integer.parseInt(copies_qty.getText().toString()))
                .backgroundColor(Color.WHITE)
                .separatorColor(Color.TRANSPARENT)
                .textColor(Color.BLACK)
                .textSize(20)
                .enableFocusability(false)
                .wrapSelectorWheel(true)
                .build();

        new AlertDialog.Builder(this)
                .setTitle("Выберите количество копий")
                .setView(picker)
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        copiesNumber = picker.getValue();
                        copies_qty.setText(copiesNumber+"");
                    }
                }).create().show();
    }

    private void doneButtonPressed() {
        String url = "http://192.168.1.4/order_mobile.php";
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {

            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.d("JSON RESPONSE!!!--=", resultResponse);
                try {
                    JSONObject result = new JSONObject(resultResponse);

                    String status = result.getString("status");
                    String message = result.getString("result");


                    // tell everybody you have succed upload image and post strings
                    if(status.equals("success")) {
                        Log.i("RESULT", message);
                    } else {
                        Log.i("Unexpected", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                baseH = mainLayout.getHeight();
                baseW = mainLayout.getWidth();

                relw = 1.0*(diffW-1)/baseW;
                relh = 1.0*(diffH-1)/baseH;

                Log.d("relw", ""+relw);
                Log.d("relh", ""+relh);

                Log.d("diffw", ""+diffW);
                Log.d("diffh", ""+diffH);

                Log.d("baseW", ""+baseW);
                Log.d("baseH", ""+baseH);

                params.put("orientation", "P");
                params.put("margin", "5");
                params.put("type", "image");
                params.put("copies", "1");
                params.put("width", ""+relw);
                params.put("height", ""+relh);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError, IOException, TransformerConfigurationException {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                Log.d("pathpath", imageName);
                params.put("file", new DataPart("image.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), new BitmapDrawable(getResources(), myBitmap)), "image/jpeg"));
                //params.put("file", new DataPart("file.docx", convertDoc(), "file/docx"));

                return params;
            }
        };
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

}
