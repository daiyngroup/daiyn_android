package com.daiyngroup.personal.qagazdaiyn;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.solver.SolverVariable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;

public class FunctionalActivity extends AppCompatActivity implements View.OnClickListener{

    Button addButton;
    Button scanButton;

    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_functional);

        addButton = (Button) findViewById(R.id.addButton);
        scanButton = (Button) findViewById(R.id.scanButton);

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        addButton.setOnClickListener(this);
        scanButton.setOnClickListener(this);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.addButton:
                chooseFileType();
                break;
            case R.id.scanButton:
                startActivity(new Intent(FunctionalActivity.this, ScannerActivity.class));
                break;
            default:
                break;
        }
    }

    private void chooseFileType() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Выберите тип файла");
//        final Intent intent = new Intent(FunctionalActivity.this, ImagePicked.class);
//        // add the buttons
//        builder.setPositiveButton("Изображения", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                intent.putExtra("file", "image");
//                startActivity(intent);
//            }
//        });
//        builder.setNegativeButton("Документы", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                intent.putExtra("file", "doc");
//                startActivity(intent);
//            }
//        });
//
//        // create and show the alert dialog
//        AlertDialog dialog = builder.create();
//        dialog.show();

        final Intent intent = new Intent(FunctionalActivity.this, ImagePicked.class);
        intent.putExtra("file", "doc");
        startActivity(intent);
//        final AlertDialog dial;
//
//        TextView docs = new TextView(this);
//        TextView imgs = new TextView(this);
//        docs.setText("Документы");
//        imgs.setText("Изображения");
//        docs.setTextColor(Color.parseColor("#225375"));
//        imgs.setTextColor(Color.parseColor("#225375"));
//
//        LinearLayout.LayoutParams docparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        LinearLayout.LayoutParams imgparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        docparams.setMargins(10, 70, 10, 30);
//        imgparams.setMargins(10, 30, 10, 70);
//        docs.setLayoutParams(docparams);
//        imgs.setLayoutParams(imgparams);
//        docs.setTypeface(null, Typeface.BOLD);
//        imgs.setTypeface(null, Typeface.BOLD);
//        docs.setGravity(View.TEXT_ALIGNMENT_CENTER);
//        imgs.setGravity(View.TEXT_ALIGNMENT_CENTER);
//        docs.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
//        imgs.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
//
//        LinearLayout linearLayout = new LinearLayout(this);
//        linearLayout.setOrientation(LinearLayout.VERTICAL);
//        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
//        linearLayout.addView(docs);
////        linearLayout.addView(imgs);
//
//        dial = new AlertDialog.Builder(this)
//                .setTitle("Выберите тип файла")
//                .setView(linearLayout)
//                .create();
//        dial.show();
//
//        docs.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dial.dismiss();
//                intent.putExtra("file", "doc");
//                startActivity(intent);
//            }
//        });
//
//        imgs.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dial.dismiss();
//                intent.putExtra("file", "image");
//                startActivity(intent);
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Выход");
        builder.setMessage("Вы уверены, что хотите выйти из приложения?");

        // add the buttons
        builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAffinity();
                }
            }
        });
        builder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
