package com.daiyngroup.personal.qagazdaiyn;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScannerActivity extends AppCompatActivity {

    int REQUEST_CODE = 99;
    int MY_REQUEST_CODE = 109;
    int MY_REQUEST_CODE1 = 110;
    ImageView scannedImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        scannedImageView = (ImageView) findViewById(R.id.scannedImageView);

        startScan();

    }

    private void startScan() {
        int preference = ScanConstants.OPEN_CAMERA;
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getContentResolver().delete(uri, null, null);
                MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "ScannedImage", "scan");
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
                Date now = new Date();
                String fileName = formatter.format(now);
                createDirectoryAndSaveFile(bitmap, "IMAGE"+fileName+".jpg");
                Toast.makeText(this, "Изображение сохранено в галерею", Toast.LENGTH_LONG).show();
                startActivity(new Intent(ScannerActivity.this, FunctionalActivity.class));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/QagazDaiyn");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/QagazDaiyn/");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/QagazDaiyn/"), fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(file);
            scanIntent.setData(contentUri);
            sendBroadcast(scanIntent);
        } else {
            final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()+"/QagazDaiyn"));
            sendBroadcast(intent);
        }

    }

}
