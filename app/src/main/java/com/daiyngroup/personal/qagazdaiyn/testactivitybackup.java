package com.daiyngroup.personal.qagazdaiyn;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.iceteck.silicompressorr.SiliCompressor;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerConfigurationException;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;

public class testactivitybackup extends AppCompatActivity implements View.OnClickListener{

    private RelativeLayout mainLayout;

    private String imageName;
    private TextView fileName;
    private SeekBar seekbar;

    private Bitmap myBitmap;

    private int finalWidth;
    private int finalHeight;

    private ImageButton copy;
    private ImageButton rotate;
    private ImageButton done;
    private ImageButton image_number;

    private boolean isRotated = true;
    private boolean isCopy = false;

    private String[] nameString;

    GridView grid;
    List<testclass> list;
    private testadapter adapter;

    private int diffH;
    private int diffW;

    private int baseH;
    private int baseW;

    private int copiesNumber;

    private double relw;
    private double relh;

    private double conrel;

    private int requestcode;

    private String[] jsonData = new String[7];

    private String device_token;
    private String orientation;

    private MaterialNumberPicker picker;
    private MaterialNumberPicker imgPicker;

    Context context;

    private int imgNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testactivity);

        fileName = (TextView) findViewById(R.id.fileName);
        seekbar = (SeekBar) findViewById(R.id.seekBar);

        copy = (ImageButton) findViewById(R.id.copy);
        rotate = (ImageButton) findViewById(R.id.orientation);
        done = (ImageButton) findViewById(R.id.done);
        image_number = (ImageButton) findViewById(R.id.img_number);

        grid = (GridView) findViewById(R.id.grid);

        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);

        device_token = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        context = this;

        Intent intent = getIntent();
        copiesNumber = 1;
        imgNumber = 1;

        imageName = intent.getStringExtra("imageName");
        requestcode = intent.getIntExtra("requestcode", 0);

        nameString = imageName.split("/");

        fileName.setText(nameString[nameString.length-1]);

        File imgFile = new File(imageName);

        if (imgFile.exists()) {
            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }

        ViewTreeObserver vto = grid.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                grid.getViewTreeObserver().removeOnPreDrawListener(this);
                finalWidth = grid.getMeasuredWidth();
                setSeekBar();
                return true;
            }
        });

        list = new ArrayList<testclass>();

        /*if (myBitmap.getHeight() > myBitmap.getWidth()) {
            myBitmap = RotateBitmap(myBitmap, -90);
        }*/

        try {
            myBitmap = SiliCompressor.with(context).getCompressBitmap(imageName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < imgNumber; i++) {
            list.add(new testclass(imageName, myBitmap.getWidth(), myBitmap.getHeight(), myBitmap));
        }

        grid.setColumnWidth(myBitmap.getWidth());

        adapter = new testadapter(this, list);

        grid.setAdapter(adapter);

    }

    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void setSeekBar() {
        rotate.setOnClickListener(this);
        copy.setOnClickListener(this);
        done.setOnClickListener(this);
        image_number.setOnClickListener(this);

        if (myBitmap.getHeight()>myBitmap.getWidth()) {
            orientation="P";
        } else {
            orientation="L";
        }

        conrel = 1.0*myBitmap.getHeight()/myBitmap.getWidth();

//        grid.setLayoutParams(new FrameLayout.LayoutParams(grid.getWidth()-grid.getHorizontalSpacing(), ViewGroup.LayoutParams.MATCH_PARENT));

        Log.d("BitmapHeight", ""+myBitmap.getHeight());
        Log.d("BitmapWidth", ""+myBitmap.getWidth());

        if (myBitmap.getWidth() > myBitmap.getHeight()) {

            if (myBitmap.getWidth() > grid.getWidth()) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setDiffW(grid.getWidth());
                    list.get(i).setDiffH((int) (list.get(i).getDiffH() - (myBitmap.getWidth() - grid.getWidth()) * conrel));
                }
                adapter.notifyDataSetChanged();
            } else {
                if (myBitmap.getWidth() < grid.getWidth()) {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).setDiffW(grid.getWidth());
                        list.get(i).setDiffH((int) (list.get(i).getDiffH() + (grid.getWidth() - myBitmap.getWidth()) * conrel));
                    }
                }
                adapter.notifyDataSetChanged();
            }

        } else {
            if (myBitmap.getHeight() > grid.getHeight()) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setDiffH(grid.getHeight());
                    list.get(i).setDiffW((int) (list.get(i).getDiffW() - (myBitmap.getHeight() - grid.getHeight()) / conrel));
                }
                adapter.notifyDataSetChanged();
            } else {
                if (myBitmap.getWidth() < grid.getWidth()) {
                    for (int i = 0; i < list.size(); i++) {
                        list.get(i).setDiffH(grid.getHeight());
                        list.get(i).setDiffW((int) (list.get(i).getDiffW() + (grid.getHeight() - myBitmap.getHeight()) / conrel));
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }

        seekbar.setMax(list.get(0).getDiffW());
        finalWidth = list.get(0).getDiffW();
        finalHeight = list.get(0).getDiffH();
        diffH = finalHeight;
        diffW = finalWidth;
        Log.d("diffW1", ""+diffW);
        Log.d("diffH1", ""+diffH);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d("conrel:", ""+conrel);
                diffW = (finalWidth-progress);
                diffH = (int) (finalHeight-progress*conrel);
               /* Log.d("diffW", ""+diffW);
                Log.d("diffH", ""+diffH);*/
                baseH = mainLayout.getHeight();
                baseW = mainLayout.getWidth();

                relw = 1.0*(diffW-1)/baseW;
                relh = 1.0*(diffH-1)/baseH;

                Log.d("relw", ""+relw);
                Log.d("relh", ""+relh);

                Log.d("diffw", ""+diffW);
                Log.d("diffh", ""+diffH);

                Log.d("baseW", ""+baseW);
                Log.d("baseH", ""+baseH);
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setDiffH(diffH);
                    list.get(i).setDiffW(diffW);
                }
                adapter.notifyDataSetChanged();
                grid.setColumnWidth(finalWidth-progress);
                grid.requestLayout();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.copy:
                copyButtonPressed();
                break;
            case R.id.orientation:
                changeOrientation();
                break;
            case R.id.done:
                //doneButtonPressed();
                doneClick();
                break;
            case R.id.img_number:
                chooseImageNumber();
                break;
            default:
                break;
        }
    }

    private void chooseImageNumber() {
        imgPicker =  new MaterialNumberPicker.Builder(this)
                .minValue(1)
                .maxValue(20)
                .defaultValue(1)
                .backgroundColor(Color.WHITE)
                .separatorColor(Color.TRANSPARENT)
                .textColor(Color.BLACK)
                .textSize(20)
                .enableFocusability(false)
                .wrapSelectorWheel(true)
                .build();

        final EditText numberText = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        numberText.setLayoutParams(lp);
        numberText.setInputType(InputType.TYPE_CLASS_NUMBER);
        numberText.setSelectAllOnFocus(true);
        numberText.setText(""+list.size());
        numberText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        new AlertDialog.Builder(this)
                .setTitle("Выберите количество картинок на листе")
                .setView(numberText)
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Integer.parseInt(numberText.getText().toString()) > 0) {
                            testclass tempClass;
                            tempClass = list.get(0);
                            list.clear();
                            for (int i = 0; i < Integer.parseInt(numberText.getText().toString()); i++) {
                                list.add(tempClass);
                                Log.d("PICKNUM", "" + list.get(i).getImagePath());
                            }
                            adapter.notifyDataSetChanged();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(numberText.getWindowToken(), 0);
                        }
                    }
                }).create().show();
    }

    private void doneClick() {
        jsonData[0] = device_token;
        jsonData[1] = orientation;
        jsonData[2] = "5";
        jsonData[3] = "image";
        jsonData[4] = ""+copiesNumber;
        jsonData[5] = ""+relw;
        jsonData[6] = ""+relh;
        Intent intent = new Intent();
        intent.putExtra("jsonvalues", jsonData);
        intent.putExtra("filetype", "image");
        setResult(RESULT_OK, intent);
        finish();
    }

    private void changeOrientation() {

        if (!isRotated) {
            myBitmap = RotateBitmap(myBitmap, 90);
            isRotated = true;
        } else {
            myBitmap = RotateBitmap(myBitmap, -90);
            isRotated = false;
        }
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setImage(myBitmap);
        }
        adapter.notifyDataSetChanged();

        int temp = finalWidth;
        finalWidth = finalHeight;
        finalHeight = temp;

        temp = diffW;
        diffW = diffH;
        diffH = temp;

        conrel = 1.0*myBitmap.getHeight()/myBitmap.getWidth();
        seekbar.setMax(finalWidth);
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setDiffH(diffH);
            list.get(i).setDiffW(diffW);
        }
        adapter.notifyDataSetChanged();
        grid.setColumnWidth(diffW);
        grid.requestLayout();
        // image.setImageBitmap(myBitmap);
    }

    private void copyButtonPressed() {
        picker =  new MaterialNumberPicker.Builder(this)
                .minValue(1)
                .maxValue(20)
                .defaultValue(1)
                .backgroundColor(Color.WHITE)
                .separatorColor(Color.TRANSPARENT)
                .textColor(Color.BLACK)
                .textSize(20)
                .enableFocusability(false)
                .wrapSelectorWheel(true)
                .build();

        new AlertDialog.Builder(this)
                .setTitle("Выберите количество копий")
                .setView(picker)
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context, "Количество копий: " + picker.getValue(), Toast.LENGTH_SHORT).show();
                        copiesNumber = picker.getValue();
                    }
                }).create().show();
    }

    private void doneButtonPressed() {
        String url = "http://192.168.1.4/order_mobile.php";
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {

            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.d("JSON RESPONSE!!!--=", resultResponse);
                try {
                    JSONObject result = new JSONObject(resultResponse);

                    String status = result.getString("status");
                    String message = result.getString("result");


                    // tell everybody you have succed upload image and post strings
                    if(status.equals("success")) {
                        Log.i("RESULT", message);
                    } else {
                        Log.i("Unexpected", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                baseH = mainLayout.getHeight();
                baseW = mainLayout.getWidth();

                relw = 1.0*(diffW-1)/baseW;
                relh = 1.0*(diffH-1)/baseH;

                Log.d("relw", ""+relw);
                Log.d("relh", ""+relh);

                Log.d("diffw", ""+diffW);
                Log.d("diffh", ""+diffH);

                Log.d("baseW", ""+baseW);
                Log.d("baseH", ""+baseH);

                params.put("orientation", "P");
                params.put("margin", "5");
                params.put("type", "image");
                params.put("copies", "1");
                params.put("width", ""+relw);
                params.put("height", ""+relh);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError, IOException, TransformerConfigurationException {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                Log.d("pathpath", imageName);
                params.put("file", new DataPart("image.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), new BitmapDrawable(getResources(), myBitmap)), "image/jpeg"));
                //params.put("file", new DataPart("file.docx", convertDoc(), "file/docx"));

                return params;
            }
        };
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

}
