package com.daiyngroup.personal.qagazdaiyn;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class PicEd extends AppCompatActivity implements View.OnClickListener{

    private String imageName;
    private TextView fileName;
    private ImageView image;
    private SeekBar seekbar;

    private Bitmap myBitmap;

    private int finalHeight;
    private int finalWidth;

    private ImageButton copy;
    private ImageButton rotate;
    private ImageButton done;

    private boolean isRotated = true;
    private boolean isCopy = false;

    private NumberPicker picker;

    private String[] nameString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_ed);

        fileName = (TextView) findViewById(R.id.fileName);
        image = (ImageView) findViewById(R.id.image);
        seekbar = (SeekBar) findViewById(R.id.seekBar);

        copy = (ImageButton) findViewById(R.id.copy);
        rotate = (ImageButton) findViewById(R.id.orientation);
        done = (ImageButton) findViewById(R.id.done);

        picker = (NumberPicker) findViewById(R.id.picker);

        picker.setMinValue(1);
        picker.setMaxValue(20);

        Intent intent = getIntent();

        imageName = intent.getStringExtra("imageName");

        nameString = imageName.split("/");

        fileName.setText(nameString[nameString.length-1]);

        File imgFile = new File(imageName);

        if (imgFile.exists()) {
            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            image.setImageBitmap(myBitmap);
        }
        Log.d("imagewidth", ""+image.getLayoutParams().width);

        ViewTreeObserver vto = image.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                image.getViewTreeObserver().removeOnPreDrawListener(this);
                finalHeight = image.getMeasuredHeight();
                finalWidth = image.getMeasuredWidth();
                setSeekBar();
                return true;
            }
        });
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void setSeekBar() {
        rotate.setOnClickListener(this);
        copy.setOnClickListener(this);
        done.setOnClickListener(this);

        seekbar.setMax(image.getWidth());
        finalWidth = image.getWidth();
        finalHeight = image.getHeight();
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                image.getLayoutParams().width = finalWidth - progress;
                image.getLayoutParams().height = finalHeight - progress;
                image.requestLayout();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.copy:
                copyButtonPressed();
                break;
            case R.id.orientation:
                changeOrientation();
                break;
            case R.id.done:
                doneButtonPressed();
                break;
            default:
                break;
        }
    }

    private void doneButtonPressed() {

    }

    private void changeOrientation() {

        if (!isRotated) {
            myBitmap = RotateBitmap(myBitmap, 90);
            isRotated = true;
        } else {
            myBitmap = RotateBitmap(myBitmap, -90);
            isRotated = false;
        }
        image.setImageBitmap(myBitmap);
    }

    private void copyButtonPressed() {
        if (!isCopy) {
            picker.setVisibility(View.VISIBLE);
            isCopy = true;
        }  else  {
            picker.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "Копий - " + picker.getValue(), Toast.LENGTH_SHORT).show();
            isCopy = false;
        }

    }
}
