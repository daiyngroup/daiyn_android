package com.daiyngroup.personal.qagazdaiyn;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iceteck.silicompressorr.SiliCompressor;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FileAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    private ArrayList<FileClass> list = new ArrayList<>();

    private String nameString;

    private Context context;

    public FileAdapter(Context context, ArrayList<FileClass> list) {
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.image_item_list, null);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.imageView);
            viewHolder.imagePath = (TextView) convertView.findViewById(R.id.fileName);
            viewHolder.delete = (ImageView) convertView.findViewById(R.id.delete);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                notifyDataSetChanged();
            }
        });
        try {
            if (list.get(position).getFileType().equals("png") || list.get(position).getFileType().equals("jpeg")||
                    list.get(position).getFileType().equals("jpg")) {
                File imgFile = list.get(position).getFile();

                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    try {
                        myBitmap = SiliCompressor.with(context).getCompressBitmap(imgFile.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    viewHolder.image.setImageBitmap(myBitmap);
                }
            } else {
                if (list.get(position).getFileType().equals("doc") || list.get(position).getFileType().equals("docx")) {
                    Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.newsmallword);
                    viewHolder.image.setImageBitmap(myBitmap);
                } else {
                    if (list.get(position).getFileType().equals("pdf")) {
                        Bitmap myBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.newsmallpdf);
                        viewHolder.image.setImageBitmap(myBitmap);
                    }
                }
            }

            nameString = list.get(position).getName();
            viewHolder.imagePath.setText(nameString);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private Bitmap checkBitmap(Bitmap bitmap) {

        return bitmap;
    }

    private class ViewHolder {
        ImageView image;
        TextView imagePath;
        ImageView delete;
    }

}
