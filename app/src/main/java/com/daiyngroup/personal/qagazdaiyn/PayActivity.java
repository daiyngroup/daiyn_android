package com.daiyngroup.personal.qagazdaiyn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class PayActivity extends AppCompatActivity {

    private List<FileClass> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        Intent intent = getIntent();
        list = (List<FileClass>) intent.getExtras().get("printvalues");
        Log.d("test", list.get(0).getName());

    }
}
