package com.daiyngroup.personal.qagazdaiyn;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class EpayFragment extends Fragment {

    private WebView webView;
    private WebSettings webSettings;

    // callbacks
    private EpayCallback successCallback;
    private EpayCallback failureCallback;

    // fields which will be send via post request
    private String email;
    private String signedOrder;

    public EpayFragment() {}

    public static EpayFragment newInstance() {
        EpayFragment fragment = new EpayFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_epay, container, false);

        this.initWebView(view);

        return view;
    }

    private void initWebView(View view) {
        webView = (WebView) view.findViewById(R.id.webView);

        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(this.getWebViewClient());

        webView.postUrl(Config.EPAY_POST_URL, buildPostData().getBytes());
    }

    private WebViewClient getWebViewClient() {
        return new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e(Config.LOG_TAG, "URL loading = " + url);
                switch (url) {
                    case Config.EPAY_FAILURE_BACK_URL:
                        failureCallback.process(url);
                        return true;
                    case Config.EPAY_BACK_URL:
                        successCallback.process(url);
                        return true;
                    case Config.EPAY_RESULT:
                        EpayActivity.isBackButtonDisabled = true;
                    case Config.EPAY_RESULT_TEST:
                        EpayActivity.isBackButtonDisabled = true;
                    default:
                        return false;
                }
            }
        };
    }

    private String buildPostData() {
        String postData = "";

        Log.e(Config.LOG_TAG, "order = " + this.signedOrder);

        try {
            postData =
                    URLEncoder.encode("Signed_Order_B64", "UTF-8") + "=" + URLEncoder.encode(this.signedOrder, "UTF-8")
                            + "&"
                            + URLEncoder.encode("BackLink", "UTF-8") + "=" + URLEncoder.encode(Config.EPAY_BACK_URL, "UTF-8")
                            + "&"
                            + URLEncoder.encode("FailureBackLink", "UTF-8") + "=" + URLEncoder.encode(Config.EPAY_FAILURE_BACK_URL, "UTF-8")
                            + "&"
                            + URLEncoder.encode("PostLink", "UTF-8") + "=" + URLEncoder.encode(Config.EPAY_POST_URL, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return postData;
    }

    // Getter and setters
    public void setSignedOrder(String signedOrder) {
        this.signedOrder = signedOrder;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSuccessCallback(EpayCallback callback) {
        this.successCallback = callback;
    }

    public void setFailureCallback(EpayCallback callback) {
        this.failureCallback = callback;
    }
}
