package com.daiyngroup.personal.qagazdaiyn;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.config.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.TransformerConfigurationException;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static com.daiyngroup.personal.qagazdaiyn.RealPathUtil.getRealPathFromURI_BelowAPI11;

public class ImagePicked extends AppCompatActivity {

    MenuItem item;

    private ProgressDialog printdialog;

    private TextView onay;
    private  TextView visa;
    private  TextView errorcardtext;
    private  TextView errorphonetext;
    TextView title;
    private  TextView orderNumText;

    private EditText cardNumber;
    private EditText phoneNumber;

    private LinearLayout numberlayout;

    private Button printsend;

    private ListView listView;
    private FileAdapter adapter;
    private ArrayList<FileClass> list;

    private Button sendButton;

    private Context context;

    private ArrayList<Bitmap> bitmapList;

    private SharedPreferences prefs;

    private ArrayList<String> imgPaths;
    private ArrayList<String> docPaths;

    private int state;

    private String[] imageValues;
    private String[] docValues;

    private boolean isSolo;

    private String device_token;

    private int pageNumber;
    private int totalcost;
    private int totalpages;
    private double longitude, latitude;

    private String order_number;
    private boolean payActivity;

    private String[] imageSettings = {"1", "1", "1", "1", "1"};
    private String[] docSettings = {"0", "1"};

    private LocationManager locationManager;

    private String emailStr;
    private String phoneStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        context = this;
        payActivity = false;
        Intent intent = getIntent();
        if (intent.getStringExtra("file").equals("image")) {
            addImages();
        } else {
            addDocs();
        }
        pageNumber = 0;
        imgPaths = new ArrayList<>();
        docPaths = new ArrayList<>();
        list = new ArrayList<>();
        adapter = new FileAdapter(this, list);
        device_token = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        docValues = new String[]{device_token,
                                "1",
                                "0",
                                "file",
                                "sequence"};

        imageValues = new String[]{device_token,
                                    "P",
                                    "5",
                                    "image",
                                    "1",
                                    "0.41487603305785126",
                                    "0.1649122807017544",
                                    "1",
                                    "1"};

        init();
    }

    private void init() {
        setContentView(R.layout.activity_image_picked);
        sendButton = (Button) findViewById(R.id.sendButton);
        listView = (ListView) findViewById(R.id.listView);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Intent intent = new Intent(ImagePicked.this, PicEd.class);
                if (list.get(position).getFileType().equals("jpg") || list.get(position).getFileType().equals("jpeg") ||list.get(position).getFileType().equals("png")) {
                    Intent intent = new Intent(ImagePicked.this, testactivity.class);
                    intent.putExtra("imageName", list.get(position).getFile().getAbsolutePath());
                    intent.putExtra("requestcode", position);
                    intent.putExtra("saved_settings", imageSettings);
                    startActivityForResult(intent, position);
                } else {
                    Intent intent = new Intent(ImagePicked.this, DocRedActivity.class);
                    intent.putExtra("filetype", list.get(position).getFileType());
                    intent.putExtra("docName", list.get(position).getName());
                    intent.putExtra("total_pages", list.get(position).getPageNum());
                    intent.putExtra("saved_settings", docSettings);
                    startActivityForResult(intent, position);
                }
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.size() == 0) {
                    Toast.makeText(ImagePicked.this, "Добавьте файлы для печати", Toast.LENGTH_SHORT).show();
                } else {
                    sendClick();
                }
            }
        });
    }

    private void addDocs() {
//        String[] docTypes = {".docx",".doc"};
//        String[] pdfTypes = {".pdf"};
//        FilePickerBuilder.getInstance().setMaxCount(32)
//                .setActivityTheme(R.style.AppTheme)
//                .enableDocSupport(false)
//                .addFileSupport("docs", docTypes, R.drawable.doc)
//                .addFileSupport("pdf", pdfTypes, R.drawable.pdf)
//                .pickFile(this);

        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(4321)

                .withFilterDirectories(true) // Set directories filterable (false by default)
                .withHiddenFiles(true) // Show hidden files and folders
                .start();

    }

    private void sendClick() {
        totalcost = 0;
        totalpages = 0;
        for (int i = 0; i < list.size(); i++) {
            Log.d("GETPAGENUM", list.get(i).getPageNum());
            if (list.get(i).getFileType().equals("jpg") || list.get(i).getFileType().equals("jpeg") || list.get(i).getFileType().equals("png")) {
                totalpages += Integer.parseInt(list.get(i).getPageNum()) * Integer.parseInt(list.get(i).getAttr()[4]);
            } else {
                if (list.get(i).getAttr()[2].equals("0")) {
                    totalpages += Integer.parseInt(list.get(i).getPageNum()) * Integer.parseInt(list.get(i).getAttr()[1]);
                } else {
                    String[] temp = list.get(i).getAttr()[2].split(",");
                    totalpages += Integer.parseInt(list.get(i).getAttr()[1]) * temp.length;
                }
            }
        }
        totalcost = totalpages * 20;
        payActivity = true;
        item.setVisible(false);
        showPopup();
//        checkPayment();
    }

    private void showPopup() {
        final AlertDialog dial;

        final EditText email = new EditText(this);
        final EditText phoneNumber = new EditText(this);
        Button accept = new Button(this);

        accept.setText("Ок");
        accept.setBackgroundColor(Color.parseColor("#2c3e50"));
        email.setText("");
        email.setHint("Почта");
        phoneNumber.setText("");
        phoneNumber.setHint("Номер телефона");

        LinearLayout.LayoutParams emailparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams phoneparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams buttonparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        emailparams.setMargins(10, 90, 10, 30);
        phoneparams.setMargins(10, 60, 10, 60);
        buttonparams.setMargins(10, 30, 10, 90);

        email.setLayoutParams(emailparams);
        phoneNumber.setLayoutParams(phoneparams);
        accept.setLayoutParams(buttonparams);

        email.setTypeface(null, Typeface.BOLD);
        phoneNumber.setTypeface(null, Typeface.BOLD);

        email.setGravity(View.TEXT_ALIGNMENT_CENTER);
        phoneNumber.setGravity(View.TEXT_ALIGNMENT_CENTER);
        accept.setGravity(View.TEXT_ALIGNMENT_CENTER);

        email.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        phoneNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        accept.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        linearLayout.addView(email);
        linearLayout.addView(phoneNumber);
        linearLayout.addView(accept);

        dial = new AlertDialog.Builder(this)
                .setTitle("Введите дополнительные данные")
                .setView(linearLayout)
                .create();
        dial.show();

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailStr = email.getText().toString();
                phoneStr = phoneNumber.getText().toString();

                if (isValidEmail(emailStr)) {
                    dial.dismiss();
                    sendEmailPhone();
                } else {
                    Toast.makeText(ImagePicked.this, "Введите верный почтовый адрес", Toast.LENGTH_SHORT).show();
                }
            }
        });
//
//        docs.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dial.dismiss();
//                intent.putExtra("file", "doc");
//                startActivity(intent);
//            }
//        });
//
//        imgs.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dial.dismiss();
//                intent.putExtra("file", "image");
//                startActivity(intent);
//            }
//        });
    }

    private void sendEmailPhone() {

        printdialog = new ProgressDialog(ImagePicked.this);
        printdialog.setTitle("Подождите");
        printdialog.setMessage("Идет подготовка заказа");
        printdialog.setCanceledOnTouchOutside(false);
        printdialog.setCancelable(false);
        printdialog.show();

        if (list.size() == 1) {
                goSendDoc(0, -2);
        } else {
            startSendingFiles();
        }
    }

    private void docPages(final File file, final String filetype) {
        String url = "http://daiyn.com/get_pages_number.php";
        final int[] pages = new int[1];
        final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {

        @Override
        public void onResponse(NetworkResponse response) {
            String resultResponse = new String(response.data);
            Log.d("JSON RESPONSE!!pages ", resultResponse);
            pageNumber += Integer.valueOf(resultResponse);
            try {
                JSONObject result = new JSONObject(resultResponse);

                String status = result.getString("status");
                String message = result.getString("result");


                // tell everybody you have succed upload image and post strings
                if (status.equals("success")) {
                    Log.i("RESULT", message);
                } else {
                    Log.i("Unexpected", message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                                    errorMessage = "Resource not found";
                                } else if (networkResponse.statusCode == 401) {
                                    errorMessage = message + " Please login again";
                                } else if (networkResponse.statusCode == 400) {
                                    errorMessage = message + " Check your inputs";
                                } else if (networkResponse.statusCode == 500) {
                                    errorMessage = message + " Something is getting wrong";
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("Error", errorMessage);
                        error.printStackTrace();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        return params;
                    }

                    @Override
                    protected Map<String, DataPart> getByteData() throws AuthFailureError, IOException, TransformerConfigurationException {
                        Map<String, DataPart> params = new HashMap<>();
                        // file name could found file base or direct access from real path
                        // for now just get bitmap data from ImageView
                        //Log.d("pathpath", list.get(i).getImagePath());
                        if (filetype.equals("pdf")) {
                            params.put("file", new DataPart("file" + 0 + ".pdf", convertDoc(file), "file/pdf"));
                        } else {
                            if (filetype.equals("doc")) {
                                params.put("file", new DataPart("file" + 0 + ".doc", convertDoc(file), "file/doc"));
                            } else {
                                if (filetype.equals("docx")) {
                                    params.put("file", new DataPart("file" + 0 + ".docx", convertDoc(file), "file/docx"));
                                }
                            }
                        }
                        return params;
                    }
                };
                VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

    private void goSendDoc(final int inew, final int statenew) {
        final int i = inew;
        final int state = statenew;
        String url = "https://daiyn.com/mobile_order_epay.php";

        final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {

            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                if (state == 1 || state == -2) {
                    order_number = resultResponse;
                    printdialog.dismiss();
                } else {
                    continueSendingFiles(i+1);
                }
                Log.d("JSON RESPONSE!!!--=", resultResponse);
                try {
                    JSONObject result = new JSONObject(resultResponse);

                    if (state == 1 || state == -2) {
                        Intent intent = new Intent(ImagePicked.this, EpayActivity.class);

                        String emailjson = result.getString("email");
                        String signed_order = result.getString("signed_order");
                        String order_id = result.getString("order_id");

                        intent.putExtra("email", emailjson);
                        intent.putExtra("signed_order", signed_order);
                        intent.putExtra("order_id", order_id);

                        startActivityForResult(intent, Config.EPAY_PAY_REQUEST);
                    }
                    String status = result.getString("status");
                    String message = result.getString("result");

                    // tell everybody you have succed upload image and post strings
                    if (status.equals("success")) {
                        Log.i("RESULT", message);
                    } else {
                        Log.i("Unexpected", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
                printdialog.dismiss();
                Toast.makeText(ImagePicked.this, "Возникла ошибка при отправлении. Отправьте еще раз", Toast.LENGTH_SHORT).show();
                init();
                payActivity = false;
//                printsend.setEnabled(true);
                item.setVisible(true);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

//                Log.d("state", ""+state);
                Log.d("type", ""+list.get(i).getFileType());
                params.put("type", list.get(i).getFileType());
                params.put("id_phone", list.get(i).getAttr()[0]);
                params.put("state", ""+state);
                params.put("copies", list.get(i).getAttr()[1]);
                params.put("email", emailStr);
                params.put("phone", phoneStr);
                Log.d("TOTAL_PAGES", ""+totalpages);

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError, IOException, TransformerConfigurationException {
                Map<String, DataPart> params = new HashMap<>();
                if (list.get(i).getFileType().equals("pdf")) {
                    params.put("file", new DataPart("file"+i+".pdf", convertDoc(list.get(i).getFile()), "file/pdf"));
                } else {
                    if (list.get(i).getFileType().equals("doc")) {
                        params.put("file", new DataPart("file"+i+".doc", convertDoc(list.get(i).getFile()), "file/doc"));
                    } else {
                        if (list.get(i).getFileType().equals("docx")) {
                            params.put("file", new DataPart("file"+i+".docx", convertDoc(list.get(i).getFile()), "file/docx"));
                        }
                    }
                }
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

    private void continueSendingFiles(int i) {
        if (i == list.size()-1) {
            state = 1;
        } else {
            state = 0;
        }
        Log.d("state", state+"");
        if (list.get(i).getFileType().equals("png") || list.get(i).getFileType().equals("jpeg") ||
                                list.get(i).getFileType().equals("jpg")) {
                            goSendPic(i, state);
                        } else {
                            goSendDoc(i, state);
                        }
    }

    private void goSendPic(final int inew, final int statenew) {
        final int i = inew;
        final int state = statenew;
        String url = "http://daiyn.com/order_mobile.php";

            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {

                @Override
                public void onResponse(NetworkResponse response) {
                    String resultResponse = new String(response.data);
                    if (state == 1 || state == -2) {
                        order_number = resultResponse;
                        printdialog.dismiss();
                        Intent intent = new Intent(ImagePicked.this, MapActivity.class);
                        intent.putExtra("ordernumber", order_number);
                        intent.putExtra("latitude", latitude);
                        intent.putExtra("longitude", longitude);
                        startActivity(intent);
                    }else {
                        continueSendingFiles(i+1);
                    }
                    Log.d("JSON RESPONSE!!!--=", resultResponse);
                    try {
                        JSONObject result = new JSONObject(resultResponse);
                        String status = result.getString("status");
                        String message = result.getString("result");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);
                            String status = response.getString("status");
                            String message = response.getString("message");

                            Log.e("Error Status", status);
                            Log.e("Error Message", message);

                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";
                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.i("Error", errorMessage);
                    error.printStackTrace();
                    printdialog.dismiss();
                    Toast.makeText(ImagePicked.this, "Возникла ошибка при отправлении. Отправьте еще раз", Toast.LENGTH_SHORT).show();
                    init();
                    payActivity = false;
                    printsend.setEnabled(true);
                    item.setVisible(true);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

//                    Log.d("state", ""+state);
                    Log.d("type", ""+list.get(i).getFileType());
                    params.put("type", list.get(i).getAttr()[3]);
                    params.put("id_phone", list.get(i).getAttr()[0]);
                    params.put("state", ""+state);
                    params.put("padding", ""+list.get(i).getAttr()[8]);
                    params.put("orientation", list.get(i).getAttr()[1]);
                    params.put("images_number", ""+list.get(i).getAttr()[7]);
                    params.put("list_copies", list.get(i).getAttr()[4]);
                    params.put("width", list.get(i).getAttr()[5]);
                    params.put("height", list.get(i).getAttr()[6]);

                    Log.d("relw", ""+ list.get(i).getAttr()[5]);
                    Log.d("relh", ""+ list.get(i).getAttr()[6]);

                    Log.d("PHONENUMBER", phoneNumber.getText().toString());
                    params.put("email", phoneNumber.getText().toString());
                    params.put("total_pages", ""+totalpages);
                    Log.d("TOTAL_PAGES", ""+totalpages);
                    return params;
                }

                @Override
                protected Map<String, DataPart> getByteData() throws AuthFailureError, IOException, TransformerConfigurationException {
                    Map<String, DataPart> params = new HashMap<>();
                    // file name could found file base or direct access from real path
                    // for now just get bitmap data from ImageView
                    //Log.d("pathpath", list.get(i).getImagePath());
                    Bitmap myBitmap = BitmapFactory.decodeFile(list.get(i).getFile().getAbsolutePath());
                    params.put("file", new DataPart("image"+i+".jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), new BitmapDrawable(getResources(), myBitmap)), "image/jpeg"));
                    return params;
                }
            };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

    private byte[] convertDoc(File file) throws TransformerConfigurationException, IOException {
        //init array with file length
        byte[] bytesArray = new byte[(int) file.length()];

        FileInputStream fis = new FileInputStream(file);
        fis.read(bytesArray); //read file into bytes[]
        fis.close();

        return bytesArray;
    }

    private void addImages() {
//        FilePickerBuilder.getInstance().setMaxCount(32)
//                .setActivityTheme(R.style.AppTheme)
//                .enableCameraSupport(false)
//                .pickPhoto(this);

        Intent intent = new Intent();
        intent.setType("image/*");
        //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Выбрать изображение"), 4343);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {

            case Config.EPAY_PAY_REQUEST:
                if (resultCode == Config.EPAY_PAY_SUCCESS) {
                    this.handlePaymentSuccess();
                }else {
                    this.handlePaymentFailure();
                }

            case FilePickerConst.REQUEST_CODE_PHOTO:
                if(resultCode== Activity.RESULT_OK && data!=null)
                {
                    imgPaths = new ArrayList<>();
                    imgPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));

                    for (int i = 0; i < imgPaths.size(); i++) {
                        File imgFile = new File(imgPaths.get(i));

                        String[] stringName = imgPaths.get(i).split("/");
                        String[] stringType = imgPaths.get(i).split("\\.");

                        Log.d("imgName", stringName[stringName.length-1]);
                        Log.d("imgType", stringType[stringType.length-1]);

                        list.add(new FileClass(imgFile,stringType[stringType.length-1] , stringName[stringName.length-1], imageValues, "1"));
                        setImages();
                    }

                }
                break;
            case FilePickerConst.REQUEST_CODE_DOC:
                if(resultCode== Activity.RESULT_OK && data!=null)
                {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));

                    if (docPaths.size() > 1) {
                        isSolo = false;
                    } else {
                        isSolo = true;
                    }

                    for (int i = 0; i < docPaths.size(); i++) {
                        File docFile = new File(docPaths.get(i));

                        String[] stringName = docPaths.get(i).split("/");
                        String[] stringType = docPaths.get(i).split("\\.");

                        Log.d("docName", stringName[stringName.length-1]);
                        Log.d("docType", stringType[stringType.length-1]);

                        SyncPages sync = new SyncPages(docFile, stringType[stringType.length-1], stringName[stringName.length-1], docValues);
                        sync.execute();

                       // Log.d("PAGENUMBER", ""+pageNumber);

                        /*list.add(new FileClass(docFile,stringType[stringType.length-1] , stringName[stringName.length-1], docValues, ""+1));
                        setImages();*/
                    }
                }
                break;
            case 4321:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                    Log.d("docpath", ""+filePath);
                    File docFile = new File(filePath);

                    String[] stringName = filePath.split("/");
                    String[] stringType = filePath.split("\\.");

                    Log.d("docName", stringName[stringName.length-1]);
                    Log.d("docType", stringType[stringType.length-1]);
                    list.add(new FileClass(docFile, stringType[stringType.length-1], stringName[stringName.length-1], docValues, ""+1));
//                    SyncPages sync = new SyncPages(docFile, stringType[stringType.length-1], stringName[stringName.length-1], docValues);
//                    sync.execute();
                }
                break;
            case 4343:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    File file = new File(data.getData().toString());
//                    String result;
//                    result = getRealPathFromURI_API19(this, data.getData());
                        String result;
                        Cursor cursor = getContentResolver().query(data.getData(), null, null, null, null);
//                        if (cursor == null) { // Source is Dropbox or other similar local file path
//                            result = data.getData().getPath();
//                        } else {
//                            if (cursor.getColumnIndex(MediaStore.Images.Media.DATA) < 0) {
//                                result = getRealPathFromURI_API19(this, data.getData());
//                            } else {
//                                cursor.moveToFirst();
//                                int idx = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
//                                result = cursor.getString(idx);
//                                cursor.close();
//                            }
//                        }

                    try {

                        result = GetPath.getPath(this, data.getData());

                        Log.d("img", "" + result);

                        File imgFile = new File(result);

                        String[] stringName = result.split("/");
                        String[] stringType = result.split("\\.");

                        Log.d("imgName", stringName[stringName.length - 1]);
                        Log.d("imgType", stringType[stringType.length - 1]);

                        list.add(new FileClass(imgFile, stringType[stringType.length - 1], stringName[stringName.length - 1], imageValues, "1"));
                        setImages();
                    } catch (NullPointerException ex) {
                        ex.printStackTrace();
                        Toast.makeText(this, "Не удалось загрузить фото", Toast.LENGTH_SHORT).show();
                    }

                }
                break;
        }

        if (requestCode < 100) {
            if (data != null) {
                Log.d("requestcode", "" + requestCode);
                if (data.getStringExtra("filetype").equals("image")) {
                    imageValues = data.getStringArrayExtra("jsonvalues");
                    imageSettings = data.getStringArrayExtra("imagesettings");
                    list.get(requestCode).setAttr(imageValues);
                    for (int i = 0; i < 5; i++) {
                        Log.d("saved settings", ""+imageSettings[i]);
                    }
                    Log.d("intent values", imageValues[0]);
                } else {
                        docValues = data.getStringArrayExtra("jsonvalues");
                        list.get(requestCode).setAttr(docValues);
                        docSettings = data.getStringArrayExtra("docsettings");
                        Log.d("doc copies number",""+docSettings[1]);
                        Log.d("doc picker value", docValues[1]);
                }
            }
        }
    }

    private void handlePaymentFailure() {

    }

    private void handlePaymentSuccess() {
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("ORDER_ID", EpayActivity.orderId);
        startActivity(intent);
    }

    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        if (uri.getHost().contains("com.android.providers.media")) {
            // Image pick from recent
            String wholeID = DocumentsContract.getDocumentId(uri);

            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        } else {
            // image pick from gallery
            return  getRealPathFromURI_BelowAPI11(context,uri);
        }

    }

    private void setImages() {
        Log.d("SETTING IMAGES", "");
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);

        item = menu.findItem(R.id.item_add);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final AlertDialog dial;
        
        int id = item.getItemId();

        if (id == R.id.item_add) {
            TextView docs = new TextView(this);
            TextView imgs = new TextView(this);
            docs.setText("Документы");
            imgs.setText("Изображения");
            docs.setTextColor(Color.parseColor("#225375"));
            imgs.setTextColor(Color.parseColor("#225375"));

            LinearLayout.LayoutParams docparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams imgparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            docparams.setMargins(10, 70, 10, 30);
            imgparams.setMargins(10, 30, 10, 70);
            docs.setLayoutParams(docparams);
            imgs.setLayoutParams(imgparams);
            docs.setTypeface(null, Typeface.BOLD);
            imgs.setTypeface(null, Typeface.BOLD);
            docs.setGravity(View.TEXT_ALIGNMENT_CENTER);
            imgs.setGravity(View.TEXT_ALIGNMENT_CENTER);
            docs.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            imgs.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
            linearLayout.addView(docs);
            linearLayout.addView(imgs);

            dial = new AlertDialog.Builder(this)
                    .setTitle("Выберите тип файла")
                    .setView(linearLayout)
                    .create();

            docs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addDocs();
                    dial.dismiss();
                }
            });

            imgs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addImages();
                    dial.dismiss();
                }
            });

            dial.show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkPayment() {
        setContentView(R.layout.activity_pay);

        String selected = "onay";

        onay = (TextView) findViewById(R.id.onay);
        visa = (TextView) findViewById(R.id.visa);
        errorcardtext = (TextView) findViewById(R.id.errorcardtext);
        errorphonetext = (TextView) findViewById(R.id.errorphonetext);
        title = (TextView) findViewById(R.id.title);
        orderNumText = (TextView) findViewById(R.id.order_number);

        cardNumber = (EditText) findViewById(R.id.cardNumber);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);

        numberlayout = (LinearLayout) findViewById(R.id.numberLayout);

        printsend = (Button) findViewById(R.id.printsend);

        //int titleprice = 10 * pageNumber;

        title.setText("Итого: "+ totalpages + " стр (" + totalcost + " тг)");

        visa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*visa.setBackgroundColor(Color.parseColor("#225375"));
                visa.setTextColor(Color.parseColor("#eeeeee"));
                onay.setBackgroundResource(R.drawable.border);
                onay.setTextColor(Color.parseColor("#225375"));*/
            }
        });

        onay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onay.setBackgroundColor(Color.parseColor("#225375"));
                onay.setTextColor(Color.parseColor("#eeeeee"));
                visa.setBackgroundResource(R.drawable.border);
                visa.setTextColor(Color.parseColor("#225375"));
            }
        });

        printsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printsend.setEnabled(false);
                printdialog = new ProgressDialog(ImagePicked.this);
                printdialog.setTitle("Подождите");
                printdialog.setMessage("Идет подготовка заказа");
                printdialog.setCanceledOnTouchOutside(false);
                printdialog.setCancelable(false);
                printdialog.show();
                if (list.size() == 1) {
                    if (list.get(0).getFileType().equals("png") || list.get(0).getFileType().equals("jpeg") ||
                            list.get(0).getFileType().equals("jpg")) {
                        goSendPic(0, -2);
                    } else {
                        goSendDoc(0, -2);
                    }
                } else {
//                    for (int i = 0; i < list.size(); i++) {
//                        Log.d("LIST SIZE", "" + list.size());
//                        if (i == 0) {
//                            state = -1;
//                        } else {
//                            if (i == list.size() - 1) {
//                                state = 1;
//                            } else {
//                                state = 0;
//                            }
//                        }
//                        //   Log.d("state", ""+state);
//                        if (list.get(i).getFileType().equals("png") || list.get(i).getFileType().equals("jpeg") ||
//                                list.get(i).getFileType().equals("jpg")) {
//                            goSendPic(i, state);
//                        } else {
//                            goSendDoc(i, state);
//                        }
//                    }
                    startSendingFiles();
                }
            }
        });

        if (selected.equals("onay")) {
            cardNumber.setVisibility(View.VISIBLE);
            cardNumber.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() < 16) {
                        errorcardtext.setVisibility(View.VISIBLE);
                        errorcardtext.setText("Введите цифры карты правильно");
                    } else {
                        if (s.length() == 16) {
                            //errorcardtext.setVisibility(View.INVISIBLE);
                            checkBalance(s);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            phoneNumber.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    if (s.length() < 12) {
//                        errorphonetext.setVisibility(View.VISIBLE);
//                        errorphonetext.setText("Введите номер правильно");
//                    } else {
//                        if (s.length() == 12) {
//                            if (s.toString().charAt(0) == '+' && s.toString().charAt(1) == '7' && s.toString().charAt(2) == '7') {
//                                printsend.setVisibility(View.VISIBLE);
//                                errorphonetext.setVisibility(View.INVISIBLE);
//                            } else {
//                                printsend.setVisibility(View.INVISIBLE);
//                                errorphonetext.setVisibility(View.VISIBLE);
//                            }
//                        }
//                    }

                    String email = phoneNumber.getText().toString();
                    if(isValidEmail(email)){
                        printsend.setVisibility(View.VISIBLE);
                        errorphonetext.setVisibility(View.INVISIBLE);
                    }else{
                        errorphonetext.setVisibility(View.VISIBLE);
                        errorphonetext.setText("Введите почту правильно");
                        printsend.setVisibility(View.INVISIBLE);
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }

    }

    private void startSendingFiles() {
        state = -1;
        Log.d("state", state+"");
        if (list.get(0).getFileType().equals("png") || list.get(0).getFileType().equals("jpeg") ||
                                list.get(0).getFileType().equals("jpg")) {
                            goSendPic(0, state);
                        } else {
                            goSendDoc(0, state);
                        }
    }

    public static boolean isValidEmail(String email)
    {
        String expression = "^[\\w\\.]+@([\\w]+\\.)+[A-Z]{2,7}$";
        CharSequence inputString = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputString);
        if (matcher.matches())
        {
            return true;
        }
        else{
            return false;
        }
    }

    final boolean checkBalance(final CharSequence s) {
        String url = "http://daiyn.com/balance_checker.php";

        final boolean[] paid = {false};

        final ProgressDialog dialog;

        dialog = new ProgressDialog(ImagePicked.this);
        dialog.setTitle("Подождите");
        dialog.setMessage("Проверка баланса");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {

            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.d("JSON RESPONSE!!!--=", resultResponse);
                dialog.dismiss();
                if (resultResponse.equals("1")) {
                    errorcardtext.setText("Средств на карте достаточно");
                    numberlayout.setVisibility(View.VISIBLE);
                    phoneNumber.requestFocus();
                } else {
                    errorcardtext.setText("Средств на карте НЕдостаточно");
                }
                try {
                    JSONObject result = new JSONObject(resultResponse);

                    String status = result.getString("status");
                    String message = result.getString("result");


                    // tell everybody you have succed upload image and post strings
                    if (status.equals("success")) {
                        Log.i("RESULT", message);
                    } else {
                        Log.i("Unexpected", message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
                dialog.dismiss();
                Toast.makeText(ImagePicked.this, "Ошибка подключения. Попробуйте еще раз", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("number", s.toString());
                params.put("sum", ""+totalcost);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError, IOException, TransformerConfigurationException {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                //Log.d("pathpath", list.get(i).getImagePath());
                return params;
            }
        };
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
        return paid[0];
    }

    public class SyncPages extends AsyncTask<Void, Void, NetworkResponse> {

        private File file;
        private String filetype;
        private int pages;
        private String filename;
        private String[] docValues;

        private ProgressDialog dialog;

        public SyncPages(File file, String filetype, String filename, String[] docValues) {
            this.file = file;
            this.filetype = filetype;
            this.filename = filename;
            this.docValues = docValues;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ImagePicked.this);
            dialog.setTitle("Подождите");
            dialog.setMessage("Идет загрузка документа");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected NetworkResponse doInBackground(Void... voids) {

            String url = "http://daiyn.com/get_pages_number.php";
            final VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {

                @Override
                public void onResponse(NetworkResponse response) {
                    String resultResponse = new String(response.data);
                    Log.d("JSON RESPONSE!!pages ", resultResponse);
                    pages = Integer.valueOf(resultResponse);
                    pageNumber = pages;
                    docValues[3] = filetype;
                    list.add(new FileClass(file, filetype, filename, docValues, ""+pageNumber));
                    setImages();
                    dialog.dismiss();
                    try {
                        JSONObject result = new JSONObject(resultResponse);

                        String status = result.getString("status");
                        String message = result.getString("result");
                        // tell everybody you have succed upload image and post strings
                        if (status.equals("success")) {
                            Log.i("RESULT", message);
                        } else {
                            Log.i("Unexpected", message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);
                            String status = response.getString("status");
                            String message = response.getString("message");

                            Log.e("Error Status", status);
                            Log.e("Error Message", message);

                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";
                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.i("Error", errorMessage);
                    error.printStackTrace();
                    dialog.dismiss();
                    Toast.makeText(ImagePicked.this, "Возникла ошибка при загрузке документа. Отправьте еще раз", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    return params;
                }

                @Override
                protected Map<String, DataPart> getByteData() throws AuthFailureError, IOException, TransformerConfigurationException {
                    Map<String, DataPart> params = new HashMap<>();
                    // file name could found file base or direct access from real path
                    // for now just get bitmap data from ImageView
                    //Log.d("pathpath", list.get(i).getImagePath());
                    if (filetype.equals("pdf")) {
                        params.put("file", new DataPart("file" + 0 + ".pdf", convertDoc(file), "file/pdf"));
                    } else {
                        if (filetype.equals("doc")) {
                            params.put("file", new DataPart("file" + 0 + ".doc", convertDoc(file), "file/doc"));
                        } else {
                            if (filetype.equals("docx")) {
                                params.put("file", new DataPart("file" + 0 + ".docx", convertDoc(file), "file/docx"));
                            }
                        }
                    }
                    return params;
                }
            };
            VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);

            return null;
        }

        @Override
        protected void onPostExecute(NetworkResponse response) {
            super.onPostExecute(response);
            Log.d("POST EXECUTE", "IM HERE");
        }
    }

    @Override
    public void onBackPressed() {
        if (payActivity) {
            init();
            payActivity = false;
            item.setVisible(true);
        } else {
            finish();
        }
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            Log.d("latitude", ""+latitude);
            Log.d("longitude", ""+longitude);
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    @Override
    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 3, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 3, 10,locationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

}