package com.daiyngroup.personal.qagazdaiyn;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class Map2Gis extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map2_gis);

        checkLocation();

        init();
    }

    private void init() {
        String mapCode = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "    <head>\n" +
                "        <title>Определение местоположения пользователя</title>\n" +
                "        <script src=\"https://maps.api.2gis.ru/2.0/loader.js\"></script>\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <div id=\"map\" style=\"width: 100%; height: 100%;\"></div>\n" +
                "        <script>\n" +
                "            DG.then(function() {\n" +
                "                var map;\n" +
                "\n" +
                "                map = DG.map('map', {\n" +
                "                    center: [54.98, 82.89],\n" +
                "                    zoom: 13\n" +
                "                });\n" +
                "\n" +
                "                map.locate({setView: true, watch: true})\n" +
                "                    .on('locationfound', function(e) {\n" +
                "                        DG.marker([e.latitude, e.longitude]).addTo(map);\n" +
                "                    })\n" +
                "                    .on('locationerror', function(e) {\n" +
                "                        DG.popup()\n" +
                "                          .setLatLng(map.getCenter())\n" +
                "                          .setContent('Доступ к определению местоположения отключён')\n" +
                "                          .openOn(map);\n" +
                "                    });\n" +
                "            });\n" +
                "        </script>\n" +
                "    </body>\n" +
                "</html>";
        WebView webview = (WebView)this.findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadDataWithBaseURL("", mapCode, "text/html", "UTF-8", "");
    }

    private boolean checkLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission. ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission. ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Определение местоположения")
                        .setMessage("Приложению требуется разрешение для определения текущего местоположения")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(Map2Gis.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission. ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission. ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        init();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

}
