package com.daiyngroup.personal.qagazdaiyn;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ErrorLaunch extends AppCompatActivity {

    Button tryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_launch);

        tryButton = (Button) findViewById(R.id.tryButton);

        tryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInternetAvailable()) {
                    startActivity(new Intent(ErrorLaunch.this, FunctionalActivity.class));
                } else {
                    showToast();
                }
            }
        });
    }

    private void showToast() {
        Toast.makeText(this, "Проверьте интернет соединение", Toast.LENGTH_SHORT).show();
    }

    public boolean isInternetAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }
        return isAvailable;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
