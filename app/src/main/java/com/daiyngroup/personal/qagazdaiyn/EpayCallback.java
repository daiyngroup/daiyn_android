package com.daiyngroup.personal.qagazdaiyn;

/**
 * Created by adilkhan on 10/1/17.
 */

public interface EpayCallback {
    public void process(Object o);
}
