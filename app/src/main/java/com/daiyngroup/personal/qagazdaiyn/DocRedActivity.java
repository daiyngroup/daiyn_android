package com.daiyngroup.personal.qagazdaiyn;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;

public class DocRedActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView fileName;
    private TextView custom_page_number;
    private TextView copies_qty;
    private TextView total_pages;

    private ImageButton copy;
    private ImageButton choose_page;
    private ImageButton done;

    private boolean isCopy = false;
    private boolean allPages = true;

    private MaterialNumberPicker picker;

    private ImageView docIcon;

    private int isSide;
    private int copiesNumber;
    private int pageNum;

    private String[] jsonData = new String[5];

    private String device_token;

    private String customPages;
    private String numberOfPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_red);

        fileName = (TextView) findViewById(R.id.fileName);
        copies_qty = (TextView) findViewById(R.id.copies_qty);
        total_pages = (TextView) findViewById(R.id.total_pages);

        copy = (ImageButton) findViewById(R.id.copy);
        done = (ImageButton) findViewById(R.id.done);
        choose_page = (ImageButton) findViewById(R.id.choose_page);
        custom_page_number = (TextView) findViewById(R.id.custom_page_number);

        //picker = (NumberPicker) findViewById(R.id.picker);

        docIcon = (ImageView) findViewById(R.id.docIcon);

        numberOfPages = "0";
        copiesNumber = 1;

        device_token = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        Intent intent = getIntent();

        if (intent.getStringExtra("filetype").equals("pdf")) {
            Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.pdf);
            docIcon.setImageBitmap(myBitmap);
        } else {
            Bitmap myBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.doc);
            docIcon.setImageBitmap(myBitmap);
        }

        pageNum = Integer.parseInt(intent.getStringExtra("total_pages"));

        fileName.setText(intent.getStringExtra("docName"));

        isSide = 1;

        total_pages.setText("Всего страниц: " + pageNum);

        copy.setOnClickListener(this);
        done.setOnClickListener(this);
        choose_page.setOnClickListener(this);

        restoreSettings();

    }

    private void restoreSettings() {
        String[] settings = getIntent().getStringArrayExtra("saved_settings");
        Log.d("settings0", settings[0]);
        Log.d("settings1", settings[1]);
        if (settings[0].equals("0") && settings[1].equals("1")) {
            Log.d("settings state", "initial settings");
        } else {
            if (settings[0].equals("0")) {
                custom_page_number.setText("Выбрано на печать: все");
            } else {
                custom_page_number.setText("Выбрано на печать: " + settings[0]);
            }

            copiesNumber = Integer.parseInt(settings[1]);
            copies_qty.setText(copiesNumber+"");
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.copy:
                copyButtonPressed();
                break;
            case R.id.choose_page:
                choosePageFunc();
                break;
            case R.id.done:
                doneClick();
                break;
            default:
                break;
        }
    }

    private void choosePageFunc() {

        TextView chooseAll = new TextView(this);
        TextView chooseSome = new TextView(this);
        chooseAll.setText("Все страницы");
        chooseSome.setText("Определенные страницы");
        chooseAll.setTextColor(Color.parseColor("#225375"));
        chooseSome.setTextColor(Color.parseColor("#225375"));

        LinearLayout.LayoutParams chooseAllParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams chooseSomeParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        chooseAllParams.setMargins(10, 70, 10, 30);
        chooseSomeParams.setMargins(10, 30, 10, 70);
        chooseAll.setLayoutParams(chooseAllParams);
        chooseSome.setLayoutParams(chooseSomeParams);
        chooseAll.setTypeface(null, Typeface.BOLD);
        chooseSome.setTypeface(null, Typeface.BOLD);
        chooseAll.setGravity(View.TEXT_ALIGNMENT_CENTER);
        chooseSome.setGravity(View.TEXT_ALIGNMENT_CENTER);
        chooseAll.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        chooseSome.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        linearLayout.addView(chooseAll);
        linearLayout.addView(chooseSome);

        final AlertDialog dial;

        dial = new AlertDialog.Builder(this)
                .setTitle("Выберите тип файла")
                .setView(linearLayout)
                .create();

        chooseAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allPages = true;
                custom_page_number.setText("Выбрано на печать: все");
                numberOfPages = "0";
                dial.dismiss();
            }
        });

        chooseSome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dial.dismiss();
                Intent intent = new Intent(DocRedActivity.this, ChoosePage.class);
                intent.putExtra("pagenum", pageNum);
                startActivityForResult(intent, 101);
            }
        });

        dial.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101 && data != null && resultCode == RESULT_OK) {
            String qty = data.getStringExtra("resultPages");
            if (!qty.equals("")) {
                customPages = qty;
                String[] choosenpages = customPages.split(",");
                int[] choosenpagesint = new int[choosenpages.length];
                for (int i = 0; i < choosenpages.length; i++) {
                    choosenpagesint[i] = Integer.parseInt(choosenpages[i]);
                    Log.d("pagesarray", ""+choosenpages[i]);
                    Log.d("pagesarrayint", ""+choosenpagesint[i]);
                }
                choosenpagesint = sortIntegerArray(choosenpagesint);
                numberOfPages = "";
                customPages = "Выбранно на печать: ";
                for (int i = 0; i < choosenpagesint.length; i++) {
                    if (i != choosenpagesint.length-1) {
                        customPages += choosenpagesint[i] + ",";
                        numberOfPages += choosenpagesint[i] + ",";
                    } else {
                        customPages += choosenpagesint[i];
                        numberOfPages += choosenpagesint[i];
                    }
                }
                if (choosenpagesint.length == pageNum) {
                    allPages = true;
                    customPages = "Выбрано на печать: все";
                    numberOfPages = "0";
                } else {
                    allPages = false;
                }

                custom_page_number.setText(customPages);
            }

        }

    }

    private int[] sortIntegerArray(int[] choosenpagesint) {
        int temp;
        boolean isSwap = true;
        while (isSwap) {
            isSwap = false;
            for (int i = 0; i < choosenpagesint.length - 1; i++) {
                if (choosenpagesint[i] > choosenpagesint[i + 1]) {
                    temp = choosenpagesint[i];
                    choosenpagesint[i] = choosenpagesint[i + 1];
                    choosenpagesint[i + 1] = temp;
                    isSwap = true;
                }
            }
        }
        return choosenpagesint;
    }

    private void doneClick() {
        jsonData[0] = device_token;
        jsonData[1] = ""+copiesNumber;
        jsonData[2] = ""+numberOfPages;
        jsonData[3] = ""+getIntent().getStringExtra("filetype");
        jsonData[4] = "sequence";
        Intent intent = new Intent();
        intent.putExtra("jsonvalues", jsonData);
        intent.putExtra("filetype", "doc");

        String[] settings = new String[2];
        settings[0] = numberOfPages;
        settings[1] = ""+copiesNumber;
        intent.putExtra("docsettings", settings);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void copyButtonPressed() {
        //if (!isCopy) {
            //picker.setVisibility(View.VISIBLE);

        picker =  new MaterialNumberPicker.Builder(this)
                .minValue(1)
                .maxValue(99)
                .defaultValue(Integer.parseInt(copies_qty.getText().toString()))
                .backgroundColor(Color.WHITE)
                .separatorColor(Color.TRANSPARENT)
                .textColor(Color.BLACK)
                .textSize(20)
                .enableFocusability(false)
                .wrapSelectorWheel(true)
                .build();

        new AlertDialog.Builder(this)
                .setTitle("Выберите количество копий")
                .setView(picker)
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        copiesNumber = picker.getValue();
                        copies_qty.setText(copiesNumber+"");
                    }
                }).create().show();
          //  isCopy = true;
        //}  else  {
          //  picker.setVisibility(View.INVISIBLE);
           // copiesText.setText("Количество копий - "+picker.getValue());
            //isCopy = false;
        //}

    }

}
