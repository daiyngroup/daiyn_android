package com.daiyngroup.personal.qagazdaiyn;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daiyngroup.personal.qagazdaiyn.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EpayActivity extends FragmentActivity {

    /*
       isBackButtonDisabled used to prevent back button press when opened payment results.
    */
    public static boolean isBackButtonDisabled = false;
    public static String orderId;
    EpayFragment epayFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_epay);

        String email = "";

        if (this.getIntent() != null) {
            email = this.getIntent().getStringExtra("email");
        }

        if (email.isEmpty()) {
            finish();
            return;
        }

        this.loadSignedOrderBase64(email);
    }

    /* Signed order loading */
    private void loadSignedOrderBase64(String email) {
        Map<String, String> params = new HashMap<>();
        params.put("email", email);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        CustomRequest jsObjRequest = new CustomRequest(
                Request.Method.POST,
                Config.SIGNED_ORDER_URL,
                params,
                this.handleSignedOrderLoadSuccess() ,
                this.handleSignedOrderLoadFailure());

        requestQueue.add(jsObjRequest);
    }

    private Response.Listener<JSONObject> handleSignedOrderLoadSuccess() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                initEpayFragment(response);
            }
        };
    }

    private Response.ErrorListener handleSignedOrderLoadFailure() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }

    /* Payment */
    private void initEpayFragment(JSONObject params) {
        try {
            String email = params.getString("email");
            String signedOrder = params.getString("signed_order");
            this.orderId = params.getString("order_id");
            this.epayFragment = EpayFragment.newInstance();

            epayFragment.setEmail(email);
            epayFragment.setSignedOrder(signedOrder);
            epayFragment.setSuccessCallback(this.handlePaymentSuccess());
            epayFragment.setFailureCallback(this.handlePaymentFailure());

            this.replaceFragment(this.epayFragment, false);
        } catch (JSONException err) {
            err.printStackTrace();
        }
    }

    private EpayCallback handlePaymentSuccess() {
        return new EpayCallback() {
            @Override
            public void process(Object o) {
                setResult(Config.EPAY_PAY_SUCCESS);
                finish();
            }
        };
    }

    private EpayCallback handlePaymentFailure() {
        return new EpayCallback() {
            @Override
            public void process(Object o) {
                setResult(Config.EPAY_PAY_FAILURE);
                finish();
            }
        };
    }

    /**
     * Replace current fragment with another fragment
     * @param f - new fragment
     * @param addToBackStack - if true current fragment will be in stack
     */
    public void replaceFragment(Fragment f, boolean addToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.replace(R.id.content, f);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (!isBackButtonDisabled) {
            setResult(Config.EPAY_PAY_FAILURE);
            super.onBackPressed();
        }
    }
}
