package com.daiyngroup.personal.qagazdaiyn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ChoosePage extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<ChoosePageClass> mDataset;

    private RelativeLayout accept_layout;
    private RelativeLayout decline_layout;

    private TextView accept;
    private TextView cancel;

    private String qty="";

    private boolean isAccepted = true;
    private int pageNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_page);

        decline_layout = (RelativeLayout) findViewById(R.id.decline_layout);
        accept_layout = (RelativeLayout) findViewById(R.id.accept_layout);

        accept = (TextView) findViewById(R.id.accept);
        cancel = (TextView) findViewById(R.id.cancel);

        decline_layout.setOnClickListener(this);
        accept_layout.setOnClickListener(this);

        Intent intent = getIntent();
        pageNumber = intent.getIntExtra("pagenum", 0);

        mDataset = new ArrayList<ChoosePageClass>();
        for (int i = 0; i < pageNumber; i++) {
            int a = i+1;
            mDataset.add(new ChoosePageClass(""+a, false));
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MainAdapter(mDataset, ChoosePage.this);
        mRecyclerView.setAdapter(mAdapter);

        cancel.setOnClickListener(this);
        accept.setOnClickListener(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("pages"));

        this.setFinishOnTouchOutside(false);

    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
                qty = intent.getStringExtra("numbers");

        }
    };

    private void sendPagesNumber(String qty) {
        if (isAccepted) {
            if (!qty.isEmpty() && !qty.equals(null)) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("resultPages", qty);
                setResult(RESULT_OK, returnIntent);
                finish();
            } else {
                Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }
        } else {
            ArrayList<String> tempList = new ArrayList<>();
            if (!qty.isEmpty()) {
                String[] tempArray = qty.split(",");
                for (int i = 0; i < pageNumber; i++) {
                    tempList.add(Integer.toString(i+1));
                }
                for (int i = 0; i < tempArray.length; i++) {
                    tempList.remove(tempArray[i]);
                }
                qty = "";
                for (int i = 0; i < tempList.size(); i++) {
                    if (i != tempList.size() - 1) {
                        qty += tempList.get(i) + ",";
                    } else {
                        qty += tempList.get(i);
                    }
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("resultPages", qty);
                setResult(RESULT_OK, returnIntent);
                finish();
            } else {
                Intent returnIntent = new Intent();
                setResult(RESULT_CANCELED, returnIntent);
                finish();
            }
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.accept_layout) {
            accept_layout.setBackgroundResource(R.drawable.kostylnyibgborder);
            decline_layout.setBackgroundResource(R.drawable.kostylnyibg);
            isAccepted = true;
        } else {
            if (id == R.id.decline_layout) {
                decline_layout.setBackgroundResource(R.drawable.kostylnyibgborder);
                accept_layout.setBackgroundResource(R.drawable.kostylnyibg);
                isAccepted = false;
            } else {
                if (id == R.id.accept) {
                    sendPagesNumber(qty);
                } else {
                    if (id == R.id.cancel) {
                        Intent returnIntent = new Intent();
                        setResult(RESULT_CANCELED, returnIntent);
                        finish();
                    }
                }
            }
        }
    }
}
