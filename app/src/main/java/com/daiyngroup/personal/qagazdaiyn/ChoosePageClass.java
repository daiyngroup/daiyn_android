package com.daiyngroup.personal.qagazdaiyn;

class ChoosePageClass {

    String number;
    boolean isChoosen;

    public ChoosePageClass(String number, boolean isChoosen) {
        this.number = number;
        this.isChoosen = isChoosen;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean isChoosen() {
        return isChoosen;
    }

    public void setChoosen(boolean choosen) {
        isChoosen = choosen;
    }
}
