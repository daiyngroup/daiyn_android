package com.daiyngroup.personal.qagazdaiyn;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

public class DocRedactAdapter extends BaseAdapter {

    private Context context;
    private List<ConstructorClass> list;
    private Bitmap myBitmap;
    private ImageView image;

    public DocRedactAdapter(Context context, List<ConstructorClass> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view== null) {
            image = new ImageView(context);
        } else {
            image = (ImageView) view;
        }

       // Picasso.with(context).load(list.get(i).getUrl()).into(target);

        Picasso.with(context)
                .load(list.get(i).getUrl())
                .placeholder(R.drawable.progress_animation)
                .into(image);

        image.setLayoutParams(new AbsListView.LayoutParams(list.get(i).getWidth(), list.get(i).getHeight()));
        return image;
    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            myBitmap = bitmap;
            float rel = (float) (1.0*myBitmap.getWidth()/myBitmap.getHeight());
            Log.d("bitmap", ""+rel);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

}