package com.daiyngroup.personal.qagazdaiyn;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private ArrayList<ChoosePageClass> mDataset;
    private ArrayList<String> pagesNumber = new ArrayList<String>();
    private Context context;

    public MainAdapter(ArrayList<ChoosePageClass> mDataset, Context context) {
        this.mDataset = mDataset;
        this.context = context;
    }

    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MainAdapter.ViewHolder holder, int position) {
        if (mDataset.get(position).isChoosen()) {
            holder.mTitle.setBackgroundResource(R.drawable.kostylnyibgborder);
        } else {
            holder.mTitle.setBackgroundResource(R.drawable.kostylnyibg);
        }
        holder.mTitle.setText(mDataset.get(position).getNumber());

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.title);
            itemView.setOnClickListener(this);
            mTitle.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            if (v.getId() == mTitle.getId()) {
                Log.d("ITEM CLICKED", "" + getAdapterPosition());
                //itemListener.recyclerViewListClicked(v, this.getLayoutPosition(), mDataset.get(this.getLayoutPosition()).isChoosen());
                //mListener.onClick(v, getAdapterPosition());
                //mListener.recyclerViewListClicked(mTitle, getAdapterPosition(), mDataset.get(getAdapterPosition()).isChoosen());
                if (mDataset.get(getAdapterPosition()).isChoosen()) {
                    mTitle.setBackgroundResource(R.drawable.kostylnyibg);
                    mDataset.get(getAdapterPosition()).setChoosen(false);
                    pagesNumber.remove(mTitle.getText().toString());
                } else {
                    mTitle.setBackgroundResource(R.drawable.kostylnyibgborder);
                    mDataset.get(getAdapterPosition()).setChoosen(true);
                    pagesNumber.add(mTitle.getText().toString());
                }
                String qty = "";
                for (int i = 0; i < pagesNumber.size(); i++) {
                    if (i != pagesNumber.size()-1) {
                        qty += pagesNumber.get(i) + ",";
                    } else {
                        qty += pagesNumber.get(i);
                    }
                }

                Intent intent = new Intent("pages");
                //            intent.putExtra("quantity",Integer.parseInt(quantity.getText().toString()));
                intent.putExtra("numbers",qty);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        }

    }

}