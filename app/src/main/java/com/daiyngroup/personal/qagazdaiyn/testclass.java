package com.daiyngroup.personal.qagazdaiyn;

import android.graphics.Bitmap;

class testclass {

    String imagePath;
    int diffH;
    int diffW;
    Bitmap image;

    public testclass(String imagePath, int diffW, int diffH, Bitmap image) {
        this.imagePath = imagePath;
        this.diffW = diffW;
        this.diffH = diffH;
        this.image = image;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getDiffH() {
        return diffH;
    }

    public void setDiffH(int diffH) {
        this.diffH = diffH;
    }

    public int getDiffW() {
        return diffW;
    }

    public void setDiffW(int diffW) {
        this.diffW = diffW;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
