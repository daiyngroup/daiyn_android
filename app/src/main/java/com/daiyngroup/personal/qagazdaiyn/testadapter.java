package com.daiyngroup.personal.qagazdaiyn;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class testadapter extends BaseAdapter {

    private Context context;
    private List<testclass> list;

    public testadapter (Context context, List<testclass> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView image;

        if (convertView == null) {
            image = new ImageView(context);
            /*File imgFile = new File(list.get(position).getImagePath());
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                image.setImageBitmap(myBitmap);
            }*/
        } else {
            image = (ImageView) convertView;
        }

        image.setImageBitmap(list.get(position).getImage());
        /*File imgFile = new File(list.get(position).getImagePath());
        Picasso.with(context).load(imgFile).fit().into(image);
        image.setId(position);*/

        image.setLayoutParams(new LinearLayout.LayoutParams(list.get(position).getDiffW(), list.get(position).getDiffH()));

        return image;
    }
}
