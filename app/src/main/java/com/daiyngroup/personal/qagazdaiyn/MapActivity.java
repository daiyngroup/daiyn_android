package com.daiyngroup.personal.qagazdaiyn;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.IMyLocationConsumer;
import org.osmdroid.views.overlay.mylocation.IMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MapActivity extends AppCompatActivity implements View.OnClickListener {

    TextView orderNumber;
    WebView webView;
    private Button returntomain;
    private Button rating_button;

    private boolean isFilled = false;

    ArrayList<OverlayItem> overlayItemArray;
    MapView map;
    Context ctx;

    Context context = this;

    GeoPoint startPoint;
    IMapController mapController;
    private Handler mHandler = new Handler();

    private LocationManager locationManager;

    private double latitude;
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.activity_map);

        clearCompressedImageFolder();

        webView = (WebView) findViewById(R.id.webview);

        orderNumber = (TextView) findViewById(R.id.order_number);

        returntomain = (Button) findViewById(R.id.return_to_main);
        rating_button = (Button) findViewById(R.id.rating_button);

        checkLocation();

        Intent intent = getIntent();
        String orderString = intent.getStringExtra("ordernumber");
        orderNumber.setText("#"+orderString);

        returntomain.setOnClickListener(this);

        map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);
        map.setUseDataConnection(true);

        MyLocationNewOverlay mLocationOverlay;

        mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(ctx),map);
        map.getOverlays().add(mLocationOverlay);
        mLocationOverlay.enableMyLocation();
        mLocationOverlay.enableFollowLocation();

        mapController = map.getController();
        mapController.setZoom(20);
        startPoint = new GeoPoint(43.2355, 76.9138);
        mapController.setCenter(startPoint);

        overlayItemArray = new ArrayList<OverlayItem>();

        OverlayItem linkopingItem = new OverlayItem("Международный Университет Информационных Технологий", "Алматы",
                new GeoPoint(43.23514, 76.90972));
        OverlayItem stockholmItem = new OverlayItem("Банк Центркредит", "Алматы",
                new GeoPoint(43.23512, 76.90787));

        overlayItemArray.add(linkopingItem);
        overlayItemArray.add(stockholmItem);

        //ItemizedIconOverlay<OverlayItem> itemizedIconOverlay = new ItemizedIconOverlay<OverlayItem>(this, overlayItemArray, null);

        Drawable marker = this.getResources().getDrawable(R.drawable.ic_marker);

        ItemizedIconOverlay<OverlayItem> itemizedIconOverlay = new ItemizedIconOverlay<OverlayItem>(overlayItemArray, marker, null, this);

        map.getOverlays().add(itemizedIconOverlay);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        handleRating();

        latitude = getIntent().getDoubleExtra("latitude", 0);
        longitude = getIntent().getDoubleExtra("longitude", 0);

        Log.d("latitude", ""+latitude);
        Log.d("longitude", ""+longitude);

        init(longitude, latitude);

    }

    private void clearCompressedImageFolder() {
        File dir = new File(Environment.getExternalStorageDirectory()+"/Silicompressor/images");
        Log.d("file path", ""+dir.getAbsolutePath());
        if (dir.isDirectory())
        {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(dir, children[i]).delete();
            }
        }
       // sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"/Silicompressor/images"))));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(("file://" + Environment.getExternalStorageDirectory()+"/Silicompressor/images")));
            sendBroadcast(intent);
        }
    }

    private void handleRating() {

        rating_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                }
            }
        });

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//            if (isFilled) {
//                rating_button.setBackgroundResource(R.drawable.stars);
//                isFilled = false;
//            } else {
//                rating_button.setBackgroundResource(R.drawable.stars_filled);
//                isFilled = true;
//            }
//
//            }
//        }, 1500);

        startRepeatingTask();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                if (isFilled) {
                    rating_button.setBackgroundResource(R.drawable.stars);
                    isFilled = false;
                } else {
                    rating_button.setBackgroundResource(R.drawable.stars_filled);
                    isFilled = true;
                }
                 //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, 1500);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission. ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission. ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Определение местоположения")
                        .setMessage("Приложению требуется разрешение для определения текущего местоположения")
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission. ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission. ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        showLocation();
                        init(54.98, 82.89);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            init(location.getLongitude(), location.getLatitude());
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    private void init(double longitude, double latitude) {
        String mapCode = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<meta charset=\"utf-8\">\n" +
                "\t<script src=\"https://maps.api.2gis.ru/2.0/loader.js\"></script>\n" +
                "</head>\n" +
                "<body>\n" +
                "\t<div id=\"map\" style=\"width: 100%; height: 400px;\"></div>\n" +
                "</body>\n" +
                "\t<script type=\"text/javascript\">\n" +
                "\t\tvar map;\n" +
                "\t\tDG.then(function() {\n" +
                "\t\t\tvar user = ["+latitude+", "+longitude+"];\n" +
                "\t\t\tvar terminals = [\n" +
                "\t\t\t\t[54.981, 82.891],\n" +
                "\t\t\t\t[54.982, 82.892],\n" +
                "\t\t\t\t[54.983, 82.893],\n" +
                "\t\t\t\t[54.984, 82.894],\n" +
                "\t\t\t\t[54.985, 82.895],\n" +
                "\t\t\t\t[54.986, 82.896]\n" +
                "\t\t\t];\n" +
                "\t\t    map = DG.map('map', {\n" +
                "\t\t        'center': user,\n" +
                "\t\t        'zoom': 16,\n" +
                "\t\t        'fullscreenControl': false,\n" +
                "\t\t        'zoomControl': false\n" +
                "\t\t    });\n" +
                "\t\t    var userIcon = DG.icon({\n" +
                "\t\t\t\t'iconUrl': 'http://daiyn.com/images/user_icon.png',\n" +
                "\t\t\t\t'iconRetinaUrl': 'http://daiyn.com/images/user_icon.png',\n" +
                "\t\t\t\t'iconSize': [32, 32]\n" +
                "\t\t\t});\n" +
                "\t\t\tvar terminalIcon = DG.icon({\n" +
                "\t\t\t\t'iconUrl': 'http://daiyn.com/images/terminal_icon.png',\n" +
                "\t\t\t\t'iconRetinaUrl': 'http://daiyn.com/images/terminal_icon.png',\n" +
                "\t\t\t\t'iconSize': [32, 32]\n" +
                "\t\t\t});\n" +
                "\t\t    DG.marker(user, {'icon': userIcon}).addTo(map);\n" +
                "\t\t    for(var i = 0; i < terminals.length; i++){\n" +
                "\t\t    \tDG.marker(terminals[i], {'icon': terminalIcon}).addTo(map);\n" +
                "\t\t    }\n" +
                "\t\t});\n" +
                "\t</script>\n" +
                "</html>";
        WebView webview = (WebView)this.findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadDataWithBaseURL("", mapCode, "text/html", "UTF-8", "");
    }

    private void showLocation() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

    @Override
    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 100, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 100, 10,locationListener);
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(MapActivity.this, FunctionalActivity.class));
    }

    @Override
    public void onBackPressed() {

    }
}
